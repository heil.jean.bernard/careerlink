package fr.eql.AI110.projet3.Puzzle.Service.implementation;

import fr.eql.AI110.projet3.Puzzle.service.SecurityServiceManagement;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class SecurityServiceImplementationTest {

    @Autowired
    SecurityServiceManagement securityServiceManagement;

    @Test
    void removeSpecialChar() {
        List<String> strings = new ArrayList<>();
        String result = "";

        strings.add("azerty");//low case OK
        strings.add("AZERTY"); //up cae OK
        strings.add("àzéèrtô"); //accent OK
        strings.add("Développeur full Stack"); //multi word
        strings.add("Dév$elop/peur full Stack"); //multi word with special char
        strings.add("a.a%a/a'a"); // not accepted
        strings.add("    "); // only space
        strings.add("null"); // null


        for (String string: strings) {
            result+=String.valueOf(securityServiceManagement.removeSpecialChar(string));
        }
        assertEquals("azertyAZERTYàzéèrtôDéveloppeur full StackDév elop peur full Stacka a a a anull",result);

    }

    @Test
    void keepOnlyNumbers() {

        List<String> numbers = new ArrayList<>();
        String result = "";

        numbers.add("10000");//min Ok
        numbers.add("999999"); //max Ok

//Invalid numbers
        numbers.add("3.3.3"); //. not accepted
        numbers.add("///444..");// /not accepted
        numbers.add("5,55"); // , not accepted
        numbers.add("9999"); // to small
        numbers.add("1000000"); // to big
        numbers.add("01200"); // start with 0
        numbers.add("null"); // null

        for (String number: numbers) {
            result=result+securityServiceManagement.isFormatSalaryIsOk(number);
        }
        assertEquals("truetruefalsefalsefalsefalsefalsefalsefalse",result);
    }

    @Test
    void isMailFormatOk() {

        List<String> emails = new ArrayList<>();
        String result = "";
        emails.add("user@domain.com");
        emails.add("user@domain.co.in");
        emails.add("user.name@domain.com");
        emails.add("user_name@domain.com");
        emails.add("username@yahoo.corporate.in");

//Invalid emails
        emails.add(".username@yahoo.com");
        emails.add("username@yahoo.com.");
        emails.add("username@yahoo..com");
        emails.add("username@yahoo.c");
        emails.add("username@yahoo.corporate");


        for (String email : emails) {
            result=result+securityServiceManagement.isMailFormatOk(email);
        }
        assertEquals("truetruetruetruetruefalsefalsefalsefalsefalse",result);
    }
    }
