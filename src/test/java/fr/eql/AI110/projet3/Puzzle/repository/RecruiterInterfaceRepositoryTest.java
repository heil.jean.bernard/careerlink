package fr.eql.AI110.projet3.Puzzle.repository;

import fr.eql.AI110.projet3.Puzzle.model.Job;
import fr.eql.AI110.projet3.Puzzle.model.Recruiter;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class RecruiterInterfaceRepositoryTest {

    @Autowired
    JobInterfaceRepository jobInterfaceRepository;

    @Autowired
    RecruiterInterfaceRepository recruiterInterfaceRepository;

    @Test
    void getByJob() {
        Job job = jobInterfaceRepository.findById(1);
        Recruiter r = recruiterInterfaceRepository.findByJob(job);

        assertEquals("Angèle", r.getFirstName());
    }
}