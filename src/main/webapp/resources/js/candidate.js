const path = document.URL.replace(location.pathname, "")

function candidateDetail(firstname, salary, adress, diploma, phone, mail, period, resume, idCandidate){


    console.log(firstname+salary+adress+diploma+phone+mail+period+resume+idCandidate)

    document.querySelector(".CandidateBlock").setAttribute("style","flex: 1; width: 60vw; margin-left: 2rem")
    document.querySelector(".candidateNameTitle").innerHTML=firstname;
    document.querySelector(".SalaryInput").innerHTML=salary;
    document.querySelector(".AdressInput").innerHTML=adress;
    document.querySelector(".DiplomaInput").innerHTML=diploma;
    document.querySelector(".PhoneInput").innerHTML=phone;
    document.querySelector(".MailInput").innerHTML=mail;
    document.querySelector(".PeriodInput").innerHTML=period;
    document.querySelector(".InputIdCandidate").innerHTML=idCandidate;
    document.querySelector(".ResumeInput").setAttribute("src",`/resources/resume/${resume}#toolbar=0`)
    console.log(`/resources/resume/${resume}`)
    document.querySelector(".DLResume").setAttribute("href",`/resources/resume/${resume}`)
}

function proposeJob(){
    const e = document.getElementById("InputJobToPropose");
    const InputJobId = e.options[e.selectedIndex].value;
    const InputCandidateId = document.querySelector(".InputIdCandidate").innerHTML
    $(document).ready(function () {
         $.ajax({
            url: path+"/actionToJob/propose",
            method: "GET",
            dataType: "text",
            data: {
                jobId: InputJobId,
                candidateId: InputCandidateId,
            }
        })
             .done(function (response) {
                 alert(JSON.stringify(response))
             })
             .fail(function (error) {
                 console.log("La requête s'est terminée en échec. Infos : " + JSON.stringify(error));
             })
    });

}


