
const path = document.URL.replace(location.pathname, "")


function candidateName(firstname, salary, adress, diploma, phone, mail, period, resume, idprocess, idStep){

    document.querySelector(".CandidateBlock"+idStep).setAttribute("style","flex: 1; width: 60vw; margin-left: 2rem")
    document.querySelector(".candidateNameTitle"+idStep).innerHTML=firstname;
    document.querySelector(".SalaryInput"+idStep).innerHTML=salary;
    document.querySelector(".AdressInput"+idStep).innerHTML=adress;
    document.querySelector(".DiplomaInput"+idStep).innerHTML=diploma;
    document.querySelector(".PhoneInput"+idStep).innerHTML=phone;
    document.querySelector(".MailInput"+idStep).innerHTML=mail;
    document.querySelector(".PeriodInput"+idStep).innerHTML=period;
    document.querySelector(".ResumeInput"+idStep).setAttribute("src",`/resources/resume/${resume}#toolbar=0`)
    console.log(`/resources/resume/${resume}`)
    document.querySelector(".DLResume"+idStep).setAttribute("href",`/resources/resume/${resume}`)
}


function setCandidateId(id) {
    $(document).ready(function () {
        $.ajax({
            url: path+"/list/setCandidate",
            method: "GET",
            dataType: "text",
            data: {
                candidateId: id,
            }
        })
            .done(function (response) {
                console.log(JSON.stringify(response))
            })
            .fail(function (error) {
                console.log("La requête s'est terminée en échec. Infos : " + JSON.stringify(error));
            })
    });

}