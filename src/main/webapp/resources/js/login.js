const PHONE_FORMAT = /(0|\+33)[1-9]( *[0-9]{2}){4}/
const MAIL_FORMAT = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
const NAME_FORMAT = /^[a-zéô]{2,}(-*)[a-zéô]*$/i
const SALARY_FORMAT = /^(?:[1-9][0-9]{4,5})$/
const PASSWORD_FORMAT = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/
const SIZE_MAX = 512000;
const path = document.URL.replace(location.pathname, "")

function closeModal() {

    $('#addAccountModal').modal('hide');
    document.querySelector(".failFields").setAttribute("style", "visibility:hidden;")
    document.querySelector(".failFields").innerHTML = "";
    document.querySelector(".firstNameInput").value = null
    document.querySelector(".lastNameInput").value = null
    document.querySelector(".mailInput").value = null
    document.querySelector(".phoneInput").value = null
    document.querySelector(".periodInput").value = null
    document.querySelector(".levelInput").value = null
    document.querySelector(".salaryInput").value = null
    document.querySelector(".passwordInput").value = null
    document.querySelector(".postalCodeInput").value = null
    document.querySelector(".cityInput").value = null
    document.getElementById("resumeInput").value = null
}

function addCandidateToDatabase() {
    let fields = new FormData()

    fields.append("firstName", document.querySelector(".firstNameInput").value)
    fields.append("lastName", document.querySelector(".lastNameInput").value)
    fields.append("mail", document.querySelector(".mailInput").value)
    fields.append("phone", document.querySelector(".phoneInput").value)
    fields.append("period", document.querySelector(".periodInput").value)
    fields.append("level", document.querySelector(".levelInput").value)
    fields.append("salary", document.querySelector(".salaryInput").value)
    fields.append("password", document.querySelector(".passwordInput").value)
    fields.append("city", document.querySelector(".cityInput").value)
    fields.append("resume", document.getElementById("resumeInput").files[0])
    fields.append("visible", document.querySelector(".visibleInput").checked)

    if (checkFields(fields)) {
        createCandidateAccount(fields)
    }
}

function checkFields(fields) {
    checkIfMailExist()

    let error = false;
    let response = "<div><strong>Erreur</strong></div><ul>";
    if (!NAME_FORMAT.test(fields.get("firstName"))) {
        response = response + "<li>Le prénom " + fields.get("firstName") + " n'est pas valide </li>";
        error = true;
    } else console.log("okFirstName");

    if (!NAME_FORMAT.test(fields.get("lastName"))) {
        response = response + "<li>Le nom " + fields.get("lastName") + " n'est pas valide </li>";
        error = true;
    } else console.log("okLastName");

    if (!MAIL_FORMAT.test(fields.get("mail"))) {
        response = response + "<li>L'adresse mail " + fields.get("mail") + " n'est pas valide </li>";
        error = true;
    } else console.log("okMail");

    if (!PHONE_FORMAT.test(fields.get("phone"))) {
        response = response + "<li>Le téléphone " + fields.get("phone") + " n'est pas valide </li>";
        error = true;
    } else console.log("okPhone");

    if (fields.get("period").length < 1) {
        response = response + "<li>Veuillez sélectionner un délai de disponibilité</li>";
        error = true;
    } else console.log("okperiod");

    if (fields.get("level").length < 1) {
        response = response + "<li>Veuillez sélectionner un niveau d'étude</li>";
        error = true;
    } else console.log("okLevel");

    if (!SALARY_FORMAT.test(fields.get("salary"))) {
        response = response + "<li>Le salaire " + fields.get("salary") + " n'est pas valide </li>";
        error = true;
    } else console.log("okSalary");

    if (fields.get("city").length < 1) {
        response = response + "<li>Veuillez sélectionner une ville</li>";
        error = true;
    } else console.log("okCity");

    if (!PASSWORD_FORMAT.test(fields.get("password"))) {
        response = response + "<li>Le mot de passe n'est pas valide </li>";
        error = true;
    } else console.log("okPassword");

    if (document.getElementById("resumeInput").files[0] != null && document.getElementById("resumeInput").files[0].size > SIZE_MAX) {
        response = response + "<li>Votre CV est trop lourd</li>";
        error = true;
    } else if (document.getElementById("resumeInput").files[0] == null) {
        response = response + "<li>Veuillez ajouter un CV</li>";
        error = true;
    } else console.log("okResume");

    if (error) {
        response = response + "</ul>"
        document.querySelector(".failFields").innerHTML = response;
        document.querySelector(".failFields").setAttribute("style", "");
        return false
    } else {
        return true
    }
}

function createCandidateAccount(fields) {

    $(document).ready(function () {
        $.ajax({
            url: path + "/candidate/createAccount",
            method: "POST",
            processData: false,
            contentType: false,
            data: fields
        })
            .done(function (response) {
                closeModal()
                let title = document.createElement("strong")
                let content = document.createElement("div")
                title.append("Succès")
                content.append(JSON.stringify(response))
                const icon = document.createElement("span")
                icon.setAttribute("class", "bficon bficon-ok")
                document.querySelector(".successAdd").innerHTML = "";
                document.querySelector(".successAdd").append(icon);
                document.querySelector(".successAdd").append(title);
                document.querySelector(".successAdd").append(content);

                document.querySelector(".successAdd").setAttribute("style", "align-text:center");

            })
            .fail(function (error) {
                console.log("La requête s'est terminée en échec. Infos : " + JSON.stringify(error));
            })
    });


}

function checkIfMailExist() {
    const mail = document.querySelector(".mailInput").value;

    $(document).ready(function () {
        $.ajax({
            url: path + "/candidate/checkIfExistAlready",
            method: "GET",
            dataType: "text",
            data: {
                mail: mail
            }
        })
            .done(function (response) {
                if (response.length > 1) {
                    console.log("Mail non disponible")
                    document.querySelector(".mailInput").value = mail + " non disponible"
                } else console.log("Mail disponible")
            })
            .fail(function (error) {
                console.log("La requête s'est terminée en échec. Infos : " + JSON.stringify(error));
            })
    });

}