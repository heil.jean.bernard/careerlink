const path = document.URL.replace(location.pathname, "")

function setJobPanel(jobTitle, minSalary, adress, diploma, time, contract, jobId, maxSalary, candidateId) {

    console.log(jobTitle + minSalary + maxSalary + adress + diploma + time + contract + " jobid : "+jobId + " candidatId "+  candidateId)

    document.querySelector(".jobBlock").setAttribute("style", "flex: 1; width: 60vw; margin-left: 2rem")
    document.querySelector(".jobTitle").innerHTML = jobTitle;
    document.querySelector(".SalaryInput").innerHTML = "entre " + minSalary + " € et " + maxSalary + " € par an";
    document.querySelector(".AdressInput").innerHTML = adress;
    document.querySelector(".DiplomaInput").innerHTML = diploma;
    document.querySelector(".TimeInput").innerHTML = time;
    document.querySelector(".ContractInput").innerHTML = contract;
    document.querySelector(".DetailInput").value = document.querySelector(".detail" + jobId).value;
    custumApplyButton(jobTitle, minSalary, adress, diploma, time, contract, jobId, maxSalary, candidateId)
}

function custumApplyButton(jobTitle, minSalary, adress, diploma, time, contract, jobId, maxSalary, candidateId) {

 if(candidateId.length<1){
     console.log("there is no candidate")
     console.log("customize apply button...")
     document.querySelector(".applyButton").innerHTML = "Connectez-vous pour postuler"
     document.querySelector(".applyButton").setAttribute("class","btn btn-success applyButton")
     document.querySelector(".applyButton").onclick = function () {
         location.href = path+"/login.xhtml";
     };
     console.log(path)

 }else {

     $(document).ready(function () {
         $.ajax({
             url: path + "/candidate/checkIfCandidateOnJob",
             method: "GET",
             dataType: "text",
             data: {
                 candidateId: candidateId,
                 jobId: jobId
             }
         })
             .done(function (response) {
                 if (response.length < 1) {
                     console.log("candidate is not on the job")
                     console.log("customize apply button...")
                     document.querySelector(".applyButton").innerHTML = "Postuler"
                     document.querySelector(".applyButton").setAttribute("class", "btn btn-success applyButton")
                     document.querySelector(".applyButton").setAttribute("onclick", `applyToJob(
         '${jobTitle}','${minSalary}','${adress}','${diploma}','${time}','${contract}','${jobId}','${maxSalary}','${candidateId}')`)

                 } else {
                     console.log("candidate already on the job")
                     console.log("changing apply button to already on the post")
                     document.querySelector(".applyButton").innerHTML = "Vous avez postulé sur ce poste"
                     document.querySelector(".applyButton").setAttribute("class", "btn btn-warning disabled applyButton")
                     return true
                 }
             })
             .fail(function (error) {
                 console.log("La requête s'est terminée en échec. Infos : " + JSON.stringify(error));
             })
     });
 }
}

function applyToJob(jobTitle, minSalary, adress, diploma, time, contract, jobId, maxSalary, candidateId) {
    console.log("Applying to job...")

    $(document).ready(function () {
        $.ajax({
            url: path + "/candidate/applyToJob",
            method: "POST",
            data: {
                candidateId:candidateId,
                jobId:jobId
            }
        })
            .done(function (response) {
console.log(response);
setJobPanel(jobTitle, minSalary, adress, diploma, time, contract, jobId, maxSalary, candidateId)
            })
            .fail(function (error) {
                console.log("La requête s'est terminée en échec. Infos : " + JSON.stringify(error));
            })
    });


}




