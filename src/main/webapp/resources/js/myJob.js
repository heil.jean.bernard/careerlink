const path = document.URL.replace(location.pathname, "")
const TITLE_FORMAT = /^[a-z0-9éô \/]*$/i
const DETAIL_FORMAT = /^[a-z0-9éô\\\- ]{2,}$/i
const SALARY_FORMAT = /^(?:[1-9][0-9]{4,5})$/
const MAIL_FORMAT = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
const NAME_FORMAT = /^[a-zéô]{2,}(-*)[a-zéô]*$/i

function jobDetail(jobTitle, minSalary, adress, diploma, time, contract, jobId, maxSalary) {

    console.log(jobTitle + minSalary + maxSalary + adress + diploma + time + contract)

    document.querySelector(".CandidateBlock").setAttribute("style", "flex: 1; width: 60vw; margin-left: 2rem")
    document.querySelector(".jobTitle").innerHTML = jobTitle;
    document.querySelector(".SalaryInput").innerHTML = "entre" + minSalary + "€ et " + maxSalary + " € par an";
    document.querySelector(".AdressInput").innerHTML = adress;
    document.querySelector(".DiplomaInput").innerHTML = diploma;
    document.querySelector(".TimeInput").innerHTML = time;
    document.querySelector(".ContractInput").innerHTML = contract;
    document.querySelector(".DetailInput").value = document.querySelector(".detail" + jobId).value;

}
function createJob() {

    let fieldsJob = new FormData()
    console.log("get field input and put in form data")
    let inputReferentId = document.querySelector(".ReferentSelectInput")
    fieldsJob.append("referentId", inputReferentId.options[inputReferentId.selectedIndex].value);

    fieldsJob.append("title", document.querySelector(".inputJobTitle").value);
    fieldsJob.append("min", document.querySelector(".inputMinSalary").value);
    fieldsJob.append("max", document.querySelector(".inputMaxSalary").value);

    let inputContractId = document.querySelector(".contractSelectInput")
    fieldsJob.append("contract", inputContractId.options[inputContractId.selectedIndex].value);

    let inputTimeId = document.querySelector(".timeSelectInput")
    fieldsJob.append("time", inputTimeId.options[inputTimeId.selectedIndex].value);

    let inputDiplomaId = document.querySelector(".diplomaSelectInput")
    fieldsJob.append("diploma", inputDiplomaId.options[inputDiplomaId.selectedIndex].value);

    fieldsJob.append("detail", document.querySelector(".jobDetailInput").value);

    if (checkFieldsJob(fieldsJob)) {
        $('.CreationModal1').modal('hide');
        document.querySelector(".failJobFields").innerHTML = "";
        console.log("Add the job to data base and update de step list in the second modal")
        addJob(fieldsJob)
    }

}
function checkFieldsJob(fieldsJob) {

    let error = false;
    let response = "<div><strong>Erreur</strong></div><ul>";

    if (!TITLE_FORMAT.test(fieldsJob.get("title"))) {
        response = response + "<li>Le titre " + fieldsJob.get("title") + " n'est pas valide </li>";
        error = true;
    } else console.log("okTitle");
    if (fieldsJob.get("contract").length < 1) {
        response = response + "<li>Veuillez sélectionner un type de contrat</li>";
        error = true;
    } else console.log("okContract");
    if (fieldsJob.get("referentId").length < 1 || fieldsJob.get("referentId") == null) {
        response = response + "<li>Veuillez sélectionner un référent</li>";
        error = true;
    } else console.log("okContract");
    if (fieldsJob.get("diploma").length < 1 || fieldsJob.get("diploma") == null) {
        response = response + "<li>Veuillez sélectionner un niveau d'étude</li>";
        error = true;
    } else console.log("okDiploma");
    if (fieldsJob.get("time").length < 1 || fieldsJob.get("time") == null) {
        response = response + "<li>Veuillez sélectionner un temps de travail</li>";
        error = true;
    } else console.log("oktime");
    if (!SALARY_FORMAT.test(fieldsJob.get("min"))) {
        response = response + "<li>Le minimum de salaire " + fieldsJob.get("min") + " n'est pas valide </li>";
        error = true;
    } else console.log("okMin");
    if (!SALARY_FORMAT.test(fieldsJob.get("max"))) {
        response = response + "<li>Le maximum de salaire " + fieldsJob.get("max") + " n'est pas valide </li>";
        error = true;
    } else console.log("okMax");
    if (fieldsJob.get("min") >= fieldsJob.get("max")) {
        response = response + "<li>Le salaire maximum est inferieur ou égal au salaire minimum </li>";
        error = true;
    } else console.log("okSalary");
    if (!DETAIL_FORMAT.test(fieldsJob.get("detail"))) {
        response = response + "<li>Le champ détails n'est pas valide </li>";
        error = true;
    } else console.log("okDetail");

console.log("error = "+error)
    if (error) {
        response = response + "</ul>"
        document.querySelector(".failJobFields").innerHTML = response;
        document.querySelector(".failJobFields").setAttribute("style", "");
        console.log("false")
        return false
    } else {
        console.log("true")
        return true
    }

}
function addJob(fieldsJob) {
    $(document).ready(function () {
        $.ajax({
            url: path + "/list/addJob",
            method: "POST",
            processData: false,
            contentType: false,
            data: fieldsJob
        })
            .done(function (response) {
                console.log("job add")
                response.map(step => {
                    $("ol#stepActualJob").append(`<li>${step ['name']}</li>`);
                })
                console.log("show modal")
                $('.CreationModal2').modal();
            })
            .fail(function (error) {
                console.log("La requête s'est terminée en échec. Infos : " + JSON.stringify(error));
            })
    });

}
function addStepToJob() {
    let fieldsStep = new FormData()

    fieldsStep.append("stepName", document.querySelector(".inputStepName").value);
    fieldsStep.append("firstName", document.querySelector(".stepFirstNameInput").value);
    fieldsStep.append("lastName", document.querySelector(".stepLastNameInput").value);
    fieldsStep.append("mail", document.querySelector(".stepMailInput").value);
    fieldsStep.append("detail", document.querySelector(".stepDetailInput").value);

    if (checkFieldsStep(fieldsStep)) {
        addStep(fieldsStep)
        document.querySelector(".failStepFields").innerHTML = "";
    }
}
function addStep(fieldsStep) {
    $(document).ready(function () {
        $.ajax({
            url: path + "/list/addStep",
            method: "POST",
            processData: false,
            contentType: false,
            data: fieldsStep
        })
            .done(function (response) {
                console.log(response);
                $("ol#stepActualJob").empty();
                response.map(step => {
                    $("ol#stepActualJob").append(`<li>${step ['name']}</li>`);
                });
            })
            .fail(function (error) {
                console.log("La requête s'est terminée en échec. Infos : " + JSON.stringify(error));
            })
    });
}
function validJob() {

    fetch(path + "/list/validate")

    document.querySelector(".failJobFields").innerHTML = "";
    document.querySelector(".inputJobTitle").value = "";
    document.querySelector(".inputMinSalary").value = "";
    document.querySelector(".inputMaxSalary").value = "";
    document.querySelector(".jobDetailInput").value = "";
    $("ol#stepActualJob").empty();
    document.querySelector(".failStepFields").innerHTML = "";
    location.reload();
}
function checkFieldsStep(fieldsStep) {
    let error = false;
    let response = "<div><strong>Erreur</strong></div><ul>";
    if (!TITLE_FORMAT.test(fieldsStep.get("stepName"))) {
        response = response + "<li>Le nom de l'étape " + fieldsStep.get("stepName") + " n'est pas valide </li>";
        error = true;
    } else console.log("okStepName");

    if (!NAME_FORMAT.test(fieldsStep.get("firstName"))) {
        response = response + "<li>Le prénom " + fieldsStep.get("firstName") + " n'est pas valide </li>";
        error = true;
    } else console.log("okStepName");

    if (!NAME_FORMAT.test(fieldsStep.get("inputLastName"))) {
        response = response + "<li>Le nom " + fieldsStep.get("LastName") + " n'est pas valide </li>";
        error = true;
    } else console.log("okStepName");

    if (!MAIL_FORMAT.test(fieldsStep.get("mail"))) {
        response = response + "<li>Le mail " + fieldsStep.get("mail") + " n'est pas valide </li>";
        error = true;
    } else console.log("okStepName");

    if (!DETAIL_FORMAT.test(fieldsStep.get("detail"))) {
        response = response + "<li>Le champ détails n'est pas valide </li>";
        error = true;
    } else console.log("okDetail");

    if (error) {
        response = response + "</ul>"
        document.querySelector(".failStepFields").innerHTML = response;
        document.querySelector(".failStepFields").setAttribute("style", "");
        return false
    } else {
        return true
    }

}








