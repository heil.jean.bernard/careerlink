const startUrl = "https://data.opendatasoft.com/api/records/1.0/search/?dataset=laposte_hexasmal%40datanova&q=";
const endUrl ="&facet=code_postal&exclude.code_commune_insee=";

function postalCodes(){
   if(document.querySelector(".postalCodeInput").value!=null) {
       const inputPostalCode = document.querySelector(".postalCodeInput").value
       fetch(startUrl + inputPostalCode + endUrl + inputPostalCode)
           .then(response => response.json())
           .then(jsonResponse => {
               document.querySelector(".postalCodeSelect").innerHTML = ``;
               let postalCodeElement = "";
               jsonResponse.records.map(record => {
                   if (postalCodeElement != record ['fields'] ['code_postal']) {
                       postalCodeElement = (record ['fields'] ['code_postal']);
                       let div = document.createElement("div");
                       div.setAttribute("class", "listElement");
                       div.setAttribute("onclick", `selectPostalCode(${postalCodeElement})`);
                       div.innerText = postalCodeElement;
                       document.querySelector(".postalCodeSelect").appendChild(div);
                   }
               })
           })
           .catch(err => console.log("une Erreur s'est produite " + err))
   }
}

function selectPostalCode(postalCode){
console.log(postalCode);

document.querySelector(".postalCodeInput").value = postalCode;
document.querySelector(".postalCodeSelect").innerHTML =``;

    const inputPostalCode = document.querySelector(".postalCodeInput").value
    fetch(startUrl+inputPostalCode+endUrl+inputPostalCode)
        .then(response => response.json())
        .then(jsonResponse => {
            console.log(jsonResponse)
            document.querySelector(".postalCodeSelect").innerHTML =``;
            document.querySelector(".citySelect").innerHTML =``;
            let cityElement = "";
            jsonResponse.records.map(record=>{
                if (cityElement != record ['fields'] ['nom_de_la_commune']) {
                    cityElement = (record ['fields'] ['nom_de_la_commune']);
                    let div = document.createElement("div");
                    div.setAttribute("class", "listElement");
                    div.setAttribute("onclick",`selectCity('${cityElement}',${inputPostalCode}, ${record ['fields'] ['coordonnees_gps'][0]}, ${record ['fields'] ['coordonnees_gps'][1]})`);
                    div.innerText = cityElement;
                    document.querySelector(".citySelect").appendChild(div);
                }
            })
        })
        .catch(err => console.log("une Erreur s'est produite "+err))
}
    function selectCity(city,codePost,latitude, longitude){
        document.querySelector(".citySelect").innerHTML =``;
        SetCityInfo(city +";"+codePost+";"+latitude+";"+longitude);


        document.querySelector(".cityInput").setAttribute("style","");
        document.querySelector(".cityInput").value= city;
    }



