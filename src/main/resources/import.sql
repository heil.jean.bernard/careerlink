LOCK TABLES `availability_period` WRITE;
/*!40000 ALTER TABLE `availability_period` DISABLE KEYS */;
INSERT INTO `availability_period` VALUES (1,'Immédiat'),(2,'1 mois'),(3,'3 mois');
/*!40000 ALTER TABLE `availability_period` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Dumping data for table `studylevel`
--

LOCK TABLES `studylevel` WRITE;
/*!40000 ALTER TABLE `studylevel` DISABLE KEYS */;
INSERT INTO `studylevel` VALUES (1,'CAP/BEP'),(2,'BAC+2'),(3,'BAC+3'),(4,'BAC+5'),(5,'BAC+8');
/*!40000 ALTER TABLE `studylevel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `workingtime`
--

LOCK TABLES `workingtime` WRITE;
/*!40000 ALTER TABLE `workingtime` DISABLE KEYS */;
INSERT INTO `workingtime` VALUES (1,'Plein-temps'),(2,'Mi-temps');
/*!40000 ALTER TABLE `workingtime` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Dumping data for table `city`
--

LOCK TABLES `city` WRITE;
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
INSERT INTO `city` VALUES (1,'Paris',48.88506977974169,2.3162117616749036,'75017'),(2,'Sèvres',48.8212407,2.2109771,'92310'),(3,'Créteil',48.790367,2.455572,'94000'),(4,'Paris',48.872490671829546,2.3123190034085677,'75008'),(5,'COLOMBES',48.9225179147,2.24675160629,'92700'),(6,'ISSY LES MOULINEAUX',48.82347434,2.26449823277,'92130'),(7,'PARIS 05',48.8445086596,2.34985938556,'75005'),(8,'BOBIGNY',48.907688244,2.43863982727,'93000'),(9,'PARIS 18',48.8927350746,2.34871193387,'75018'),(10,'LA CELLE SOUS GOUZON',46.2188855114,2.20927433054,'23230'),(11,'PARIS 17',48.8873371665,2.30748555949,'75017'),(12,'ASNIERES SUR SEINE',48.9153530123,2.2880384663,'92600'),(13,'MELUN',48.5444723553,2.65795821917,'77000'),(14,'ARRONVILLE',49.1740736985,2.1128755699,'95810'),(15,'EVRY COURCOURONNES',48.6294831659,2.44008244492,'91000'),(16,'VERSAILLES',48.8025669671,2.11789297191,'78000'),(17,'IVRY SUR SEINE',48.8128063812,2.38778023612,'94200');
/*!40000 ALTER TABLE `city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `company`
--

LOCK TABLES `company` WRITE;
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
INSERT INTO `company` VALUES (1,'Famous tech company',NULL,NULL,'Techplus','362 521 879 00034',1),(2,'Great ESN',NULL,NULL,'SmartTechConsulting','415 685 895 00018',1),(3,'Hiring place',NULL,NULL,'ILoveRecruit','852 456 369 00017',1);
/*!40000 ALTER TABLE `company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `contracttype`
--

LOCK TABLES `contracttype` WRITE;
/*!40000 ALTER TABLE `contracttype` DISABLE KEYS */;
INSERT INTO `contracttype` VALUES (1,'CDI'),(2,'CDD'),(3,'Interimaire'),(4,'Apprentissage');
/*!40000 ALTER TABLE `contracttype` ENABLE KEYS */;
UNLOCK TABLES;



--
-- Dumping data for table `candidate`
--

LOCK TABLES `candidate` WRITE;
/*!40000 ALTER TABLE `candidate` DISABLE KEYS */;
INSERT INTO `candidate` VALUES (1,'2022-02-22','Aline','3f25e207e1ef0403b39a372591c51e1859143e0c888ca2247727d5a0f950ea1b','DUPUY','a.dupuy@gmail.com','0525859878',45000,_binary '',2,11,4),(2,'2022-02-22','Lucia','71cd3f66610796897fcbb49dbc190efc270189a8ba4ef4effe18f3f71365a1b5','NGO','l.ngo@gmail.com','0652589878',65000,_binary '',1,12,4),(3,'2022-02-22','Paul','3dd6a47fe485eb8b4aafb2bc494e26b3568f347c3eb42d16cbee258eeb22ce96','DUPONT','p.dupont@hotmail.fr','0754849858',38000,_binary '',3,4,4),(4,'2022-02-22','Lucie','6ab54f1f9fa22f49ca430fa55aaf5155fd7dded4ad462d460df0f6251f375278','DUBOIS','l.dubois@gmail.com','0758989565',36000,_binary '',1,13,3),(5,'2022-02-22','Roland','c9f617d3337b6fa563541c5a4045179e4edca33b92649d475ccab5e808442a49','DUPUIS','r.dupuis@gmail.com','0625859878',35000,_binary '',2,14,5),(6,'2022-02-22','Victor','50101093fb76f987d2634b5c52b8e079782d5e1740f665948d8599e273d7a495','DEMBLIN','v.demblin@gmail.com','0632589878',78000,_binary '',3,15,4),(7,'2022-02-22','Nicolas','024e829d33e48b100613f1344169ba4596823480e8f0273e12d72bd28ebb3e4b','LAMARRE','n.lamarre@hotmail.fr','0623525898',30000,_binary '',2,16,3),(8,'2022-02-22','Fabienne','6060e15a00e60148f853c8432e294f1fd376af07639fc431810b2d25ab186a11','MORIN','fabmorrin@outlook.com','0658589565',39000,_binary '',1,17,4);
/*!40000 ALTER TABLE `candidate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `resume`
--

LOCK TABLES `resume` WRITE;
/*!40000 ALTER TABLE `resume` DISABLE KEYS */;
INSERT INTO `resume` VALUES (1,'CV Aline DUPUY.pdf',1),(2,'CV Lucia NGO.pdf',2),(3,'CV Paul DUPONT.pdf',3),(4,'CV Lucie DUBOIS.pdf',4),(5,'CV Roland DUPUIS.pdf',5),(6,'Victor DEMBLIN.pdf',6),(7,'Nicolas Lamarre.pdf',7),(8,'CV Fabienne MORIN.pdf',8);
/*!40000 ALTER TABLE `resume` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Dumping data for table `recruiter`
--

LOCK TABLES `recruiter` WRITE;
/*!40000 ALTER TABLE `recruiter` DISABLE KEYS */;
INSERT INTO `recruiter` VALUES (1,NULL,'Angèle','4be1e23f685f1538c99edf21637c3855134e61b2358bd5376b7bcb739b5e3deb','AUCLAIR','a.auclair@techplus.fr',NULL,1,1),(2,NULL,'Rémi','338b244ebcbb1ead9747451c05a14ede222a630b633b6a2e2701849bef25b735','LELOUP','r.leloup@stc.com',NULL,3,2),(3,NULL,'Wilfried','9b89a2cb9dbdeeef6d71f1be929f275b616ad0daeac0ef462d8333e37163e1ec','CASIN','casinw@iloverecruit.com',NULL,6,3);
/*!40000 ALTER TABLE `recruiter` ENABLE KEYS */;
UNLOCK TABLES;



--
-- Dumping data for table `job`
--

INSERT INTO `job` VALUES (1,NULL,'2022-02-18','Votre Quête Journalière\n\nAu sein du studio de production de Paris, vous intégrerez l\'équipe de développement des outils d\'un jeu AAA, composée d\'une dizaine de développeurs expérimentés.\n\nEn tant que Développeur C#/.Net (H/F/NB), vous serez responsable de la conception et du développement de ces outils (client lourd ou léger), qui sont utilisés par l\'ensemble des développeurs et artistes de la production du jeu.\n\nPlus précisément, vos missions seront les suivantes :\n\nConcevoir et développer ces outils en C# / WPF  : développement du code des éditeurs et de nouvelles interfaces graphiques,\nAnalyser les difficultés et besoins des utilisateurs afin de proposer et développer des solutions concrètes d’amélioration ou d\'optimisation de leurs outils de travail,\nA travers de la veille, proposer des choix de technologies et/ou d\'architectures pour les outils développés.\nQUALIFICATIONS\nVos skills :\n\nDe formation supérieure de type école d’ingénieur ou équivalent, vous avez un minimum de 3 ans d’expérience en programmation C# / WPF,\nVous avez des connaissances sur des outils de versioning type Perforce ou Git,\nVous êtes sensible à l\'amélioration de l’expérience utilisateur,\nVous êtes autonome, agissez de manière proactive et vous avez un bon sens relationnel,\nVotre niveau d‘anglais vous permet d’échanger facilement dans un contexte international.',NULL,68000,45000,'Développeur C# H/F',1,1,1,1,1),(6,NULL,'2022-02-17','Les missions du poste :\n•	Rédiger les spécifications techniques\n•	Développer les nouvelles fonctionnalités\n•	Effectuer les tests unitaires\n•	Assurer la maintenance corrective et évolutive de l’application\n•	Participer à l’intégration et au déploiement de l’application\nEnvironnement technique : Java 11, Git, Microservices, Kafka, API, Docker, BDD...\nType d\'emploi : Temps plein, CDI\nStatut : Cadre\nSalaire : 38 000,00€ à 48 000,00€ par an\nAvantages :\n•	Participation au Transport\n•	RTT\n•	Titre-restaurant\nHoraires :\n•	Du Lundi au Vendredi\n\n',NULL,48000,38000,'Développeur back-end H/F',6,1,1,4,1),(7,NULL,'2022-02-17','Nous recherchons dans le cadre de notre fort développement, un(e) :\nDéveloppeur Java (H/F)\nVous aurez pour principales missions :\n- Réaliser des interconnexions avec des systèmes externes (caisses enregistreuses, EDI Fournisseurs, Logiciels de paie)\n- Echanger régulièrement avec nos clients pour sans cesse améliorer ces interconnexions\n- Maintenir et enrichir une API publique pour faciliter les échanges avec notre logiciel\n- Tests fonctionnels\n- Participer à l\'architecture et aux choix technologiques\nA ce titre vous participerez à toutes les étapes du projet, de l’élaboration des spécifications techniques, des maquettes jusqu’au développement.\nProfil technique recherché :\nDe formation Bac+2/5, vous avez 3 ans d’expérience minimum sur un poste similaire.\nVous recherchez aujourd\'hui un nouveau challenge professionnel et souhaitez intégrer une équipe à taille humaine.\n- Maîtrise de Java\n- Vous produisez du code structuré (DRY), lisible, maintenable\n- Vous avez déjà écrit des tests unitaires\nCompétences optionnelles :\n- Capacité à écrire des scripts en python\n- Technologie web (HTML, CSS, Javascript + Frameworks)\nCompétences comportementales :\n- Méthodique, rigoureux\n- Vous êtes doté d\'un esprit ouvert et communiquant\n- Vous avez l\'esprit d\'équipe\n- Vous saurez échanger régulièrement avec nos clients (mail et téléphone)\nNous travaillons de façon flexible, sommes ouverts au travail à distance\nStatut : Cadre\nType d\'emploi : Temps plein, CDI\nSalaire : à partir de 45 000,00€ par an\nAvantages :\n•	Horaires flexibles\n•	Participation au Transport\n•	Titre-restaurant\n•	Travail à Distance\nHoraires :\n•	Du Lundi au Vendredi\n•	Périodes de Travail de 8 Heures\n•	Repos le Week-end\n•	Travail en journée\nFormation:\n•	Bac +2 (BTS, DUT, DEUG) (Exigé)\nExpérience:\n•	Java: 3 ans (Exigé)\nLangue:\n•	Français (Exigé)\nTélétravail:\n•	Oui',NULL,50000,45000,'Développeur Java (H/F)',7,1,1,3,1),(8,NULL,'2022-02-17','Le candidat intégrera le service IT Broadcast constitué d’une dizaine de personnes aux compétences variées, en effet notre service intervient sur une multitude de problématiques/projets broadcast innovants tels que :\n•	\nPlatform d’insertions graphiques\n•	Production de l’auto-promo d’Eurosport\n•	Portail commentateurs permettant de commenter des événements sportifs depuis chez soi\n•	Plateforme de livraison de contenus VOD chez nos affiliés (Canal+, Amazon Prime, Sky…)\nChaque membre du service intervient sur un ou plusieurs sujets listés ci-dessus et les projects team sont constituées de développeurs .NET Core, NodeJS, React...ainsi que d’un product owner/business analyst. Nous travaillons également en étroite collaboration avec d’autres équipes telles que le network, Infrastructure, info/Sec & DBA.\nNos plateformes sont essentiellement hébergées sur le cloud AWS. Nous utilisons d’ailleurs une large palette de services natifs proposées par AWS tels que S3, EFS, ECS, EC2, ALB, DynamoDB..avec un soin tout particulier porté sur la \"Résilience” et “Redundancy” de nos plateformes.\nLe candidat retenu pour ce poste devra faire preuve d’autonomie, être curieux, créatif et d’une grande habileté en résolution de problèmes sur les sujets qui lui seront confiés.\n\nResponsibilities:\nLes missions principales :\n•	\nRecueillir et analyser les besoins métiers en partenariat avec nos Products Owners\n•	Maintenir les applications existantes et avoir la charge du développement de nouvelles fonctionnalités\n•	Être Force de propositions innovantes sur les sujets confiés\n•	Travailler en équipe et partager vos connaissances, en contribuant à l’amélioration continue des méthodes et des outils.\n\n\nQualifications:\nProfil souhaité :\n•	Bac +5 (Ecole d’ingénieur en informatique ou Master en informatique)\n•	De préférence une expérience significative (> 3 ans) dans le développement Web Full-Stack.\n•	Anglais courant\nConnaissances et compétences techniques: NET Core (c#), AWS (basic knowledge), SQL, Entity framework code first, Queries / Stored procedure, Rest API\n\nLe vrai plus :\n•	Connaissance en Architecture\n•	Notions en réseau\n•	MVC\n•	NoSQL\nSavoir être :\n•	Créatif\n•	Curieux\n•	Bonne communication\n•	Proactif\n\n','2022-02-23',78000,55000,'Full Stack Developer H/F',8,1,3,4,1),(9,NULL,'2022-02-17','Pour accompagner notre croissance, et l’ouverture de notre site web, nous recherchons un Assistant Commercial/ Développer Web (H/F), dans le cadre d’une alternance.\nMissions\nDirectement rattaché(e) à la dirigeante de la société, votre mission est globale :\n- Tenue du site web en construction\n- Gestion des commandes e-commerce\n- Mise à jour catalogue\n- Mise en place de tests suite à la mise en ligne et fonctionnement du site\n- Assurer la formation permettant l’utilisation du site et notamment la publication de nouveaux contenus (textes, médias…).\n- Construction d’offres sur mesure et rédaction des devis\n- Participation à la création de produits et de nos d’offres de service\nCette liste n\'est pas exhaustive, nous attendons quelqu\'un de proactif et dynamique capable de prendre de l\'envergure au sein de la structure.\nProfil\nDe formation Bac + 2/3 minimum, vous avez idéalement une première expérience de développement ou de téléacteur. Vous avez un sens relationnel aigu et avez la capacité à vous adresser à des interlocuteurs de tout niveau. Vous avez l’esprit de conquête, la culture du challenge. Vous êtes autonome et souhaitez participer au développement d\'une structure à fort potentiel.\nDoté d’un grand professionnalisme, vous êtes empathique et curieux ce qui vous permet de gagner la confiance d’interlocuteurs exigeants.\nType d\'emploi : Temps plein, Alternance\nSalaire : à partir de 1 570,00€ par mois\nAvantages :\n•	Participation au Transport\n•	Titre-restaurant\nHoraires :\n•	Du Lundi au Vendredi\n•	Repos le Week-end\nQuestion(s) de présélection:\n•	donner un de vos traits de personnalité\nExpérience:\n•	Développement Web: 1 an (Optionnel)\n•	Informatique: 1 an (Exigé)\n•	HTML5: 1 an (Optionnel)\nLangue:\n•	Anglais (Optionnel)\nTélétravail:\n•	Non',NULL,18000,12000,'Developpeur Web en alternance H/F',9,4,2,3,1);


--
-- Dumping data for table `step`
--

LOCK TABLES `step` WRITE;
/*!40000 ALTER TABLE `step` DISABLE KEYS */;
INSERT INTO `step` VALUES (1,'Analyse des CV reçus','Candidatures à traiter ','Jean-Bernard','heil.jb@hotmail.fr','HEIL',2,1),(2,'En attente d\'un retour candidat','Proposition envoyée ','Jean-Bernard','heil.jb@hotmail.fr','HEIL',19,1),(3,'Candidats non validés','Sortie','Jean-Bernard','heil.jb@hotmail.fr','HEIL',100,1),(4,'Candidat recruté','Recruté','Jean-Bernard','heil.jb@hotmail.fr','HEIL',20,1),(5,'Premier entretien RH avec le Candidat','Entretien RH','Jean-Bernard','heil.jb@hotmail.fr','HEIL',4,1),(6,'Test technique à distance','Test technique','Jean-Bernard','heil.jb@hotmail.fr','HEIL',5,1),(7,'Entretien avec le responsable technique ','Entretien technique','Jean-Bernard','heil.jb@hotmail.fr','HEIL',6,1),(8,'Sortie de process.','Sortie','Jean-Bernard','heil.jb@hotmail.fr','HEIL',100,6),(9,'Le poste à été proposé à un candidat et ce dernier doit répondre.','Attente retours candidat','Jean-Bernard','heil.jb@hotmail.fr','HEIL',1,6),(10,'Liste des candidatures reçues','Candidatures à traiter','Jean-Bernard','heil.jb@hotmail.fr','HEIL',2,6),(11,'Une proposition de recrutement à été envoyée','Proposition envoyée','Jean-Bernard','heil.jb@hotmail.fr','HEIL',19,6),(12,'Le candidat est recruté','Candidat recruté','Jean-Bernard','heil.jb@hotmail.fr','HEIL',20,6),(13,'Entretien RH sur les motivations','Entretien RH','Isabelle','ileloup@hotmail.fr','Leloup',3,6),(14,'Entretien technique et passage d\'un test','Entretien Technique','Paul','pdupuis@careerLink.com','Dupuis',4,6),(15,'Sortie de process.','Sortie','Jean-Bernard','heil.jb@hotmail.fr','HEIL',100,7),(16,'Le poste à été proposé à un candidat et ce dernier doit répondre.','Attente retours candidat','Jean-Bernard','heil.jb@hotmail.fr','HEIL',1,7),(17,'Liste des candidatures reçues','Candidatures à traiter','Jean-Bernard','heil.jb@hotmail.fr','HEIL',2,7),(18,'Une proposition de recrutement à été envoyée','Proposition envoyée','Jean-Bernard','heil.jb@hotmail.fr','HEIL',19,7),(19,'Le candidat est recruté','Candidat recruté','Jean-Bernard','heil.jb@hotmail.fr','HEIL',20,7),(20,'Entretien Rh sur les motivation','Entretien RH','Rosalie','rkurzwac@careerlink.fr','Kurzwac',3,7),(21,'Test technique à distance validé par une responsable du pôle développement.','Test technique','Julie','jsavignolle@careerlink.fr','Savignolle',3,7),(22,'Sortie de process.','Sortie','Jean-Bernard','heil.jb@hotmail.fr','HEIL',100,8),(23,'Le poste à été proposé à un candidat et ce dernier doit répondre.','Attente retours candidat','Jean-Bernard','heil.jb@hotmail.fr','HEIL',1,8),(24,'Liste des candidatures reçues','Candidatures à traiter','Jean-Bernard','heil.jb@hotmail.fr','HEIL',2,8),(25,'Une proposition de recrutement à été envoyée','Proposition envoyée','Jean-Bernard','heil.jb@hotmail.fr','HEIL',19,8),(26,'Le candidat est recruté','Candidat recruté','Jean-Bernard','heil.jb@hotmail.fr','HEIL',20,8),(27,'Entretien autours de l\'expérience technique','Entretien Technique','Jean','j.durand@careerlink.fr','Durand',3,8),(28,'Entretien RH','Entretien RH','Louise','l.legrand@careerlink.fr','Legrand',3,8),(29,'Sortie de process.','Sortie','Jean-Bernard','heil.jb@hotmail.fr','HEIL',100,9),(30,'Le poste à été proposé à un candidat et ce dernier doit répondre.','Attente retours candidat','Jean-Bernard','heil.jb@hotmail.fr','HEIL',1,9),(31,'Liste des candidatures reçues','Candidatures à traiter','Jean-Bernard','heil.jb@hotmail.fr','HEIL',2,9),(32,'Une proposition de recrutement à été envoyée','Proposition envoyée','Jean-Bernard','heil.jb@hotmail.fr','HEIL',19,9),(33,'Le candidat est recruté','Candidat recruté','Jean-Bernard','heil.jb@hotmail.fr','HEIL',20,9),(34,'Entretien RH de motivation','Entretien RH','Adrien','alouis@careerlink.fr','Louis',3,9);
/*!40000 ALTER TABLE `step` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `recruitmentprocess`
--

INSERT INTO `recruitmentprocess` VALUES (1,'Le candidat à postulé via la recherche de poste','2022-02-23',NULL,'2022-02-23',1,1,1),(2,'Le candidat à postulé via la recherche de poste','2022-02-23',NULL,'2022-02-23',1,1,10),(3,'Le candidat à postulé via la recherche de poste',NULL,'2022-02-23','2022-02-23',1,1,17),(4,'Le responsable n\'a pas validé la candidature à l\'étape précédente',NULL,NULL,'2022-02-23',1,1,3),(5,'Le candidat à postulé via la recherche de poste',NULL,NULL,'2022-02-23',2,2,1),(6,'Le candidat à postulé via la recherche de poste','2022-02-23',NULL,'2022-02-23',2,2,10),(7,'Le candidat à postulé via la recherche de poste',NULL,'2022-02-23','2022-02-23',2,2,17),(8,'Le candidat à postulé via la recherche de poste',NULL,NULL,'2022-02-23',2,2,24),(9,'Le candidat à postulé via la recherche de poste',NULL,NULL,'2022-02-23',4,4,31),(10,'Le candidat à postulé via la recherche de poste',NULL,'2022-02-23','2022-02-23',4,4,24),(11,'Le candidat à postulé via la recherche de poste',NULL,NULL,'2022-02-23',4,4,17),(12,'Le candidat à postulé via la recherche de poste',NULL,NULL,'2022-02-23',4,4,10),(13,'Le candidat à postulé via la recherche de poste',NULL,NULL,'2022-02-23',7,7,1),(14,'Le candidat à postulé via la recherche de poste',NULL,NULL,'2022-02-23',7,7,10),(15,'Le candidat à postulé via la recherche de poste',NULL,NULL,'2022-02-23',7,7,17),(16,'Le candidat à postulé via la recherche de poste','2022-02-23',NULL,'2022-02-23',7,7,24),(17,'poste proposé par le recruteur',NULL,NULL,'2022-02-23',1,1,30),(18,'poste proposé par le recruteur',NULL,NULL,'2022-02-23',6,6,9),(19,'Le responsable à validé la candidature à l\'étape précédente',NULL,'2022-02-23','2022-02-23',1,1,20),(20,'Le responsable à validé la candidature à l\'étape précédente',NULL,NULL,'2022-02-23',2,2,20),(21,'Le responsable à validé la candidature à l\'étape précédente',NULL,NULL,'2022-02-23',1,1,21),(22,'Le responsable n\'a pas validé la candidature à l\'étape précédente',NULL,NULL,'2022-02-23',7,7,22),(23,'Le responsable à validé la candidature à l\'étape précédente',NULL,'2022-02-23','2022-02-23',4,4,27),(24,'Le responsable à validé la candidature à l\'étape précédente',NULL,'2022-02-23','2022-02-23',4,4,28),(25,'Le responsable à validé la candidature à l\'étape précédente',NULL,'2022-02-23','2022-02-23',4,4,25),(26,'Le responsable à validé la candidature à l\'étape précédente',NULL,NULL,'2022-02-23',4,4,26),(27,'Le responsable n\'a pas validé la candidature à l\'étape précédente',NULL,NULL,'2022-02-23',1,1,8),(28,'Le responsable n\'a pas validé la candidature à l\'étape précédente',NULL,NULL,'2022-02-23',2,2,8);



