package fr.eql.AI110.projet3.Puzzle.service;

import fr.eql.AI110.projet3.Puzzle.model.Candidate;
import fr.eql.AI110.projet3.Puzzle.model.City;
import fr.eql.AI110.projet3.Puzzle.model.Job;
import fr.eql.AI110.projet3.Puzzle.model.RecruitmentProcess;
import org.apache.el.parser.BooleanNode;

import java.util.List;

public interface JobServiceInterface {

    boolean isActiveJob(Job job);
    List<Job> getAllJob();
    List<Job> getActiveJobs();
    List<Job> getJobsByCandidateId(Candidate candidate);
    List<Job> getJobsByRecruiterId(long id);
    Job getById(long id);
    Job saveJob(Job job);
    List<RecruitmentProcess> recrutedProcessOnThisJob(Job job);
    Job createJob(Job job);
    List<Job> filterJobsByCityAndRadius(List<Job> jobs, City city, int radius);
    List<Job> filterJobs(
            Integer min,
            Integer max,
            Long studyLevelId,
            Long workingTimeId,
            Long contractTypeId,
            String title,
            String details);
}
