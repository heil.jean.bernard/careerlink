package fr.eql.AI110.projet3.Puzzle.repository;

import fr.eql.AI110.projet3.Puzzle.model.Candidate;
import fr.eql.AI110.projet3.Puzzle.model.Job;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface JobInterfaceRepository extends JpaRepository<Job, Long> {

    List<Job> findAll();
    List<Job> findAllByCloseDateIsNullAndEffectiveHiringDateIsNull();
    @Query("SELECT j FROM Job j INNER JOIN FETCH j.stepSet s WHERE j.id =:id")
    Job findById(@Param("id") long id);

    List<Job> findByRecruiterId(long id);

    @Query("SELECT DISTINCT j FROM Job j " +
            "INNER JOIN  j.stepSet s " +
            "INNER JOIN  s.recruitmentProcessSet r " +
            "INNER JOIN  r.candidate " +
            "WHERE r.candidate=:idCandidate")
    List<Job> findByCandidateId(@Param("idCandidate") Candidate candidate);

    @Query("SELECT j FROM Job j WHERE ( " +
            "(j.maxSalary<=:max OR (:max IS NULL)) AND " +
            "(j.minSalary>=:min OR (:min IS NULL)) AND " +
            "(j.studyLevel.id = :studyLevelId OR (:studyLevelId IS NULL)) AND " +
            "(j.workingTime.id = :workingTimeId OR (:workingTimeId IS NULL))AND" +
            "(j.contractType.id = :contractTypeId OR (:contractTypeId IS NULL))AND" +
            "(j.title like %:title% OR (:title IS NULL) OR (:title = ''))AND" +
            "(j.details like %:details% OR (:details IS NULL) OR (:details = '')))  "
    )
    List<Job> findByMulticriteria(
            @Param(value = "min") Integer min,
            @Param(value = "max") Integer max,
            @Param(value = "studyLevelId") Long studyLevelId,
            @Param(value = "workingTimeId") Long workingTimeId,
            @Param(value = "contractTypeId") Long contractTypeId,
            @Param(value = "title") String title,
            @Param(value = "details") String details);
}
