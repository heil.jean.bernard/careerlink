/**
 * This controller is for action relative to candidates
 * Each method is connected to a functionality
 *
 * @version 1.0
 *
 * @see fr.eql.AI110.projet3.Puzzle.service.CandidateServiceInterface
 * @see fr.eql.AI110.projet3.Puzzle.controller.CandidateRestController
 * @author Jean-Bernard HEIL
 */

package fr.eql.AI110.projet3.Puzzle.controller;

import fr.eql.AI110.projet3.Puzzle.model.*;
import fr.eql.AI110.projet3.Puzzle.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import static java.lang.Long.parseLong;

@Controller(value = "mbCandidate")
@Scope(value = "session")
public class CandidateManagedBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private String firstName;
    private String lastName;
    private String mail;
    private String password;
    private String phoneNumber;
    private int salary;
    private String cityName;
    private String latitude;
    private String longitude;
    private String codePost;
    private long availabilityPeriodId;
    private long studyLevelId;
    private boolean visible;
    private City city;
    private int radius;
    private List<Candidate> candidates;
    private List<AvailabilityPeriod> periods;
    private Long periodId;
    private Long levelId;
    private Integer minSalary;
    private Integer maxSalary;
    private List<StudyLevel> levels;


    @Autowired
    private CandidateServiceInterface candidateServiceInterface;
    @Autowired
    private AvailabilityServiceInterface availabilityServiceInterface;
    @Autowired
    private StudyLevelServiceInterface studyLevelServiceInterface;
    @Autowired
    private CityServiceInterface cityServiceInterface;
    @Autowired
    private ResumeServiceInterface resumeServiceInterface;
    @Autowired
    private SecurityServiceManagement securityServiceManagement;

    //region Methods

    @PostConstruct
    public void init() {
        periods = availabilityServiceInterface.getAllAvailabilityPeriods();
        candidates = getAllVisibleCandidate();
        periods = availabilityServiceInterface.getAllAvailabilityPeriods();
        levels = studyLevelServiceInterface.getAllStudyLevel();

    }

    public String filterCandidate() {

        maxSalary = securityServiceManagement.isFormatSalaryIsOk(String.valueOf(maxSalary))?maxSalary:null;
        minSalary = securityServiceManagement.isFormatSalaryIsOk(String.valueOf(minSalary))?maxSalary:null;


        String cityTest = city != null ? city.getCityName() : "null";
        System.out.println("min = :" + minSalary + " maxSalary :" + maxSalary + " level id : " + levelId + " periodid :" + periodId + "city :" + cityTest);
        candidates = candidateServiceInterface.getAllFiltredCandidate(minSalary, maxSalary, levelId, periodId);
        minSalary = null;
        maxSalary = null;
        levelId = null;
        periodId = null;

        if (city != null) {
            candidates = candidateServiceInterface.filterCandidatesByCityAndRadius(candidates, city, radius);
        }

        city = null;

        return "/candidate.xhtml?faces-redirection=true";
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public List<AvailabilityPeriod> getPeriods() {
        return periods;
    }

    public void setPeriods(List<AvailabilityPeriod> periods) {
        this.periods = periods;
    }

    public List<Candidate> getAllVisibleCandidate() {
        return candidateServiceInterface.getAllVisibleCandidate();
    }

    public String addCandidate() {

        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        city = (City) session.getAttribute("city");

        Candidate candidate = new Candidate();
        candidate.setCreationDate(LocalDate.now());
        candidate.setCity(city);
        candidate.setFirstName(firstName);
        candidate.setLastName(lastName);
        candidate.setMail(mail);
        candidate.setVisible(visible);
        candidate.setSalary(salary);
        candidate.setPhoneNumber(phoneNumber);
        candidate.setStudyLevel(studyLevelServiceInterface.getById(studyLevelId));
        candidate.setAvailabilityPeriod(availabilityServiceInterface.getById(availabilityPeriodId));

        candidateServiceInterface.create(candidate);

        session.removeAttribute("city");
        return "/login.xhtml?faces-redirection=true";
    }

    public void setCityInfo() {
        System.out.println("Start setting city in session");
        System.out.println("Get city info from request");
        Map<String, String> requestParamMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String[] infoCity = requestParamMap.get("CityInfo").split(";");

        System.out.println("Checking if city already exist ...");
        if (cityServiceInterface.getCityByPostCodeAndCityName(infoCity[1],infoCity[0]) != null) {
            System.out.println("City already exist, getting city infos");
            city = cityServiceInterface.getCityByPostCodeAndCityName(infoCity[1],infoCity[0]);
        } else {
            System.out.println("City don't exist already in data base, saving city info");
            city = cityServiceInterface.saveCity(new City(infoCity[0], infoCity[1], Double.parseDouble(infoCity[3]), Double.parseDouble(infoCity[2])));
        }

        System.out.println("Adding city in session...");
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        session.setAttribute("city", city);

        System.out.println("Check if city is in the session : ");
        City test = (City) session.getAttribute("city");
        System.out.println("The city in the session is : "+test.getCityName()+" "+test.getPostCode());

        System.out.println("City add to session complete.");
    }

    public Resume getFirstResumeByCandidateId(long id) {
        return resumeServiceInterface.getAllResumeByIdCandidate(id).get(0);
    }

    public void deleteAddInfo() {
        lastName = null;
        firstName = null;
        phoneNumber = null;
        mail = null;
        city = null;
        salary = 0;
        availabilityPeriodId = 0;
        studyLevelId = 0;
    }


    public List<Candidate> getAll() {
        return candidateServiceInterface.getAllCandidate();
    }
    //endregion

    //region Accessors

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getSalary() {
        return salary;
    }

    public List<StudyLevel> getLevels() {
        return levels;
    }

    public void setLevels(List<StudyLevel> levels) {
        this.levels = levels;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCodePost() {
        return codePost;
    }

    public void setCodePost(String codePost) {
        this.codePost = codePost;
    }

    public long getAvailabilityPeriodId() {
        return availabilityPeriodId;
    }

    public void setAvailabilityPeriodId(long availabilityPeriodId) {
        this.availabilityPeriodId = availabilityPeriodId;
    }

    public long getStudyLevelId() {
        return studyLevelId;
    }

    public void setStudyLevelId(long studyLevelId) {
        this.studyLevelId = studyLevelId;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public List<Candidate> getCandidates() {
        return candidates;
    }

    public void setCandidates(List<Candidate> candidates) {
        this.candidates = candidates;
    }

    public Long getPeriodId() {
        return periodId;
    }

    public void setPeriodId(Long periodId) {
        this.periodId = periodId;
    }

    public Long getLevelId() {
        return levelId;
    }

    public void setLevelId(Long levelId) {
        this.levelId = levelId;
    }

    public Integer getMinSalary() {
        return minSalary;
    }

    public void setMinSalary(Integer minSalary) {
        this.minSalary = minSalary;
    }

    public Integer getMaxSalary() {
        return maxSalary;
    }

    public void setMaxSalary(Integer maxSalary) {
        this.maxSalary = maxSalary;
    }


    //endregion
}
