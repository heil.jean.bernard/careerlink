package fr.eql.AI110.projet3.Puzzle.repository;

import fr.eql.AI110.projet3.Puzzle.model.Candidate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CandidateInterfaceRepository extends JpaRepository<Candidate, Long> {

    @Query("SELECT o FROM Candidate o WHERE o.mail=:mailParam AND o.hashedPassword=:passwordParam")
    Candidate authenticate(@Param(value = "mailParam") String login, @Param(value = "passwordParam") String password);

    Candidate findById(long id);
    Candidate findByMailAndHashedPassword(String mail, String hashedPassword);
    List<Candidate> findAllByVisibleIsTrue();

    @Query("SELECT c FROM Candidate c WHERE ( c.visible is true AND (c.salary<=:max OR (:max IS NULL)) AND (c.salary>=:min OR (:min IS NULL)) AND (c.studyLevel.id = :diplomaId OR (:diplomaId IS NULL)) AND (c.availabilityPeriod.id = :periodId OR (:periodId IS NULL)))"
    )
    List<Candidate>findByMulticriteria(@Param(value = "min") Integer min,@Param(value = "max")  Integer max,@Param(value = "diplomaId")  Long diplomaId,@Param(value = "periodId")  Long periodId);

    Candidate findByMail(String mail);

}

