/**
 * This class is using as a referential to reference the city for a candidate
 * the longitude and latitude are used in geolocation
 *
 * @version 1.0
 *
 * @see fr.eql.AI110.projet3.Puzzle.model.Candidate
 * @see fr.eql.AI110.projet3.Puzzle.model.Job
 * @see fr.eql.AI110.projet3.Puzzle.model.Recruiter
 * @see fr.eql.AI110.projet3.Puzzle.model.Company
 *
 * @author Jean-Bernard HEIL
 */

package fr.eql.AI110.projet3.Puzzle.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table (name = "city")
public class City {
    //region Properties
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "cityname")
    private String cityName;
    @Column(name = "postcode")
    private String postCode;
    @Column(name = "longitude")
    private double longitude;
    @Column(name = "latitude")
    private double latitude;
    //endregion

    //region Relational

    @OneToMany(mappedBy = "city", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Recruiter> recruiterSet;
    @OneToMany(mappedBy = "city", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Company> companySet;
    @OneToMany(mappedBy = "city", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Job> jobSet;
    @OneToMany(mappedBy = "city", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Candidate> candidateSet;


    //endregion

    //region Methods

    //endregion

    //region Constructors
    public City() {
    }

    public City(String cityName, String postCode, double longitude, double latitude) {
        this.cityName = cityName;
        this.postCode = postCode;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public City(Long id, String cityName, String postCode, double longitude, double latitude, Set<Recruiter> recruiterSet, Set<Company> companySet, Set<Job> jobSet, Set<Candidate> candidateSet) {
        this.id = id;
        this.cityName = cityName;
        this.postCode = postCode;
        this.longitude = longitude;
        this.latitude = latitude;
        this.recruiterSet = recruiterSet;
        this.companySet = companySet;
        this.jobSet = jobSet;
        this.candidateSet = candidateSet;
    }
    //endregions

    //region Accessors
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public Set<Recruiter> getRecruiterSet() {
        return recruiterSet;
    }

    public void setRecruiterSet(Set<Recruiter> recruiterSet) {
        this.recruiterSet = recruiterSet;
    }

    public Set<Company> getCompanySet() {
        return companySet;
    }

    public void setCompanySet(Set<Company> companySet) {
        this.companySet = companySet;
    }

    public Set<Job> getJobSet() {
        return jobSet;
    }

    public void setJobSet(Set<Job> jobSet) {
        this.jobSet = jobSet;
    }

    public Set<Candidate> getCandidateSet() {
        return candidateSet;
    }

    public void setCandidateSet(Set<Candidate> candidateSet) {
        this.candidateSet = candidateSet;
    }
    //endregion
}