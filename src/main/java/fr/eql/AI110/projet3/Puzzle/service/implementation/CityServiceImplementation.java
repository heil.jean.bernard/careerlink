package fr.eql.AI110.projet3.Puzzle.service.implementation;

import fr.eql.AI110.projet3.Puzzle.model.City;
import fr.eql.AI110.projet3.Puzzle.repository.CityInterfaceRepository;
import fr.eql.AI110.projet3.Puzzle.service.CityServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CityServiceImplementation implements CityServiceInterface {

    @Autowired
    private CityInterfaceRepository cityInterfaceRepository;

    @Override
    public City saveCity(City city) {
        cityInterfaceRepository.save(city);
        return city;
    }

    @Override
    public City getCityByPostCodeAndCityName(String postCode, String cityName) {
      return  cityInterfaceRepository.findByPostCodeAndCityName(postCode, cityName);
    }
}
