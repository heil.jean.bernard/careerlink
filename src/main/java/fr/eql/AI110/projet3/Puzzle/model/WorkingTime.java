/**
 * This class is using as a referential to reference all the working time selectable in the application (Full-time, Part-time,...)
 *
 * @version 1.0
 *
 * @see fr.eql.AI110.projet3.Puzzle.model.Job

 *
 * @author Jean-Bernard HEIL
 */

package fr.eql.AI110.projet3.Puzzle.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "workingtime")
public class WorkingTime {
    //region Properties
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "type")
    private String type;
    //endregion

    //region Relational
    @OneToMany(mappedBy = "workingTime", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Job> jobSet;
    //endregion

    //region Methods

    //endregion

    //region Constructors
    public WorkingTime(Long id, String type, Set<Job> jobSet) {
        this.id = id;
        this.type = type;
        this.jobSet = jobSet;
    }

    public WorkingTime() {
    }
    //endregion

    //region Accessors
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Set<Job> getJobSet() {
        return jobSet;
    }

    public void setJobSet(Set<Job> jobSet) {
        this.jobSet = jobSet;
    }
    //endregion
}
