package fr.eql.AI110.projet3.Puzzle.service;

import fr.eql.AI110.projet3.Puzzle.model.StudyLevel;

import java.util.List;

public interface StudyLevelServiceInterface {

    List<StudyLevel> getAllStudyLevel();
    StudyLevel getById(long id);
    StudyLevel getByLevel(String level);
}
