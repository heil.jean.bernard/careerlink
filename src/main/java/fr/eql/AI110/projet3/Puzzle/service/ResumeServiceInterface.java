package fr.eql.AI110.projet3.Puzzle.service;

import fr.eql.AI110.projet3.Puzzle.model.Resume;

import java.util.List;

public interface ResumeServiceInterface {

    List<Resume> getAllResume();
    Resume getByid(long id);
    List<Resume> getAllResumeByIdCandidate(long id);
    Resume createResume(Resume resume);

}
