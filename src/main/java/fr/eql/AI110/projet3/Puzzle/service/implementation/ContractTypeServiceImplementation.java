package fr.eql.AI110.projet3.Puzzle.service.implementation;

import fr.eql.AI110.projet3.Puzzle.model.ContractType;
import fr.eql.AI110.projet3.Puzzle.repository.ContractTypeInterfaceRepository;
import fr.eql.AI110.projet3.Puzzle.service.ContractTypeServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContractTypeServiceImplementation implements ContractTypeServiceInterface {

    @Autowired
    private ContractTypeInterfaceRepository contractTypeInterfaceRepository;


    @Override
    public List<ContractType> getAllContractType() {
        return contractTypeInterfaceRepository.findAll();
    }

    @Override
    public ContractType getByType(String type) {
        return contractTypeInterfaceRepository.findByType(type);
    }

    @Override
    public ContractType getById(long id) {
        return contractTypeInterfaceRepository.findById(id);
    }
}
