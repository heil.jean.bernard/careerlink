/**
 * Method use
 *
 * @version 1.0
 *
 * @author Jean-Bernard HEIL
 */

package fr.eql.AI110.projet3.Puzzle.controller;


import fr.eql.AI110.projet3.Puzzle.model.*;
import fr.eql.AI110.projet3.Puzzle.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.util.*;

@Slf4j
@RestController
@RequestMapping("/list")
public class StepRestController {

    @Autowired
    StepServiceInterface stepServiceInterface;
    @Autowired
    private JobServiceInterface jobServiceInterface;
    @Autowired
    private StudyLevelServiceInterface studyLevelServiceInterface;
    @Autowired
    private WorkingTimeServiceInterface workingTimeServiceInterface;
    @Autowired
    private ContractTypeServiceInterface contractTypeServiceInterface;
    @Autowired
    private RecruiterServiceInterface recruiterServiceInterface;
    @Autowired
    private CandidateServiceInterface candidateServiceInterface;


    @PostMapping("/addJob")
    public List<Step> saveJobAndReturnStepList(
            HttpSession session,
            @RequestParam String referentId,
            @RequestParam String title,
            @RequestParam String min,
            @RequestParam String max,
            @RequestParam String contract,
            @RequestParam String time,
            @RequestParam String diploma,
            @RequestParam String detail
    ) {

        log.info("start adding job to database");
        log.debug("cheking if there is a city in session..");

        if (session.getAttribute("city") != null) {
            System.out.println(referentId+title+ min+
                    max+
                    contract+
                    time+
                    diploma+
                    detail);
            log.debug("city is in session, saving new job..");
            City city = (City) session.getAttribute("city");
            Job job = jobServiceInterface.createJob(new Job(
                    null,
                    title,
                    detail,
                    Integer.parseInt(max),
                    Integer.parseInt(min),
                    LocalDate.now(),
                    null,
                    null,
                    city,
                    recruiterServiceInterface.getById(Long.parseLong(referentId)),
                    studyLevelServiceInterface.getById(Long.parseLong(diploma)),
                    contractTypeServiceInterface.getById(Long.parseLong(contract)),
                    workingTimeServiceInterface.getById(Long.parseLong(time)),
                    null));

            log.debug("add job {} to session",job.getTitle());
            session.setAttribute("jobSave", job);

            log.debug("remove city from session");
            session.removeAttribute("city");

            log.info("job add complete, return stepList");
            log.debug("getting step list from job without step 1 and step 100");
            return stepServiceInterface.getStepListWithoutStep1AndStep100(job);
        }
        log.info("no city in session, saving job failed, return null");
        return null;
    }

    @PostMapping("/addStep")
    public List<Step> addStepAndReturnUpdatedStepList(
            HttpSession session,
            @RequestParam String stepName,
            @RequestParam String firstName,
            @RequestParam String lastName,
            @RequestParam String detail,
            @RequestParam String mail
    ) {

        log.debug("getting job from session");
        Job jobToAddStep = (Job) session.getAttribute("jobSave");
        log.info("Create step : {} and add to job : {}", stepName,jobToAddStep.getTitle());
        stepServiceInterface.addStepToJob(jobToAddStep,stepName,firstName,lastName,detail,mail);

        log.info("step add complete, return stepList");
        log.debug("getting step list from job without step 1 and step 100");
        return stepServiceInterface.getStepListWithoutStep1AndStep100(jobToAddStep);
    }



    @GetMapping("/validate")
    public void StepListOfCurrentJob(HttpSession session){
        session.removeAttribute("jobSave");

    }


    @GetMapping("/setCandidate")
    public String StepListOfCurrentJob(
            HttpSession session,
            @RequestParam String candidateId){
        Candidate candidate = candidateServiceInterface.getById(Long.parseLong(candidateId));
        session.setAttribute("currentCandidate",candidate);

        Candidate candidate1 = (Candidate) session.getAttribute("currentCandidate");

        return "le candidat :"+candidate1.getFirstName()+" "+candidate1.getLastName()+" a été ajouté en session";
    }

}
