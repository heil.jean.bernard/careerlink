package fr.eql.AI110.projet3.Puzzle.repository;

import fr.eql.AI110.projet3.Puzzle.model.Job;
import fr.eql.AI110.projet3.Puzzle.model.Recruiter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RecruiterInterfaceRepository extends JpaRepository<Recruiter, Long> {

    @Query("SELECT r FROM Recruiter r WHERE r.mail=:mailParam AND r.hashedPassword=:passwordParam")
    Recruiter authenticate(@Param(value = "mailParam") String login, @Param(value = "passwordParam") String password);

    Recruiter findByMailAndHashedPassword(String mail, String hashedPassword);
    Recruiter findById(long id);
    List<Recruiter> findAllByCompanyId(Long id);

    @Query("SELECT r FROM Recruiter r INNER JOIN r.jobSet j WHERE j=:job ")
    Recruiter findByJob(Job job);

}

