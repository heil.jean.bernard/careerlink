/**
 * This controller is relative to job functionality
 *
 * @version 1.0
 *
 * for other functionality relatives to job
 * @see fr.eql.AI110.projet3.Puzzle.controller.JobRestController
 * @author Jean-Bernard HEIL
 */


package fr.eql.AI110.projet3.Puzzle.controller;

import fr.eql.AI110.projet3.Puzzle.model.*;
import fr.eql.AI110.projet3.Puzzle.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Controller(value = "mbJob")
@Scope(value = "session")
public class JobManagedBean implements Serializable {

    private List<Job> jobs;
    private List<Job> activeJobs;
    private List<Job> userJobs;
    private Recruiter recruiter;
    private Candidate candidate;
    private Job job = new Job();

    private String title;
    private String details;
    private Integer maxSalary;
    private Integer minSalary;
    private LocalDate creationDate;
    private LocalDate effectiveHiringDate;
    private LocalDate closeDate;
    private StudyLevel studyLevel;
    private ContractType contractType;
    private WorkingTime workingTime;
    private City city;
    private Long contractTypeId;
    private Long workingTimeId;
    private Long studyLevelId;
    private Long referentId;
    private int radius = 0;

    private String filterMinSalary;
    private String filterMaxSalary;
    private String filterStudyLevel;
    private String filterWorkingTime;
    private String filterContractType;
    private String filterTitle;
    private String filterDetails;
    private String filterCity;
    private String filterRadius;

    private List<WorkingTime> times;
    private List<StudyLevel> levels;
    private List<ContractType> contracts;
    private List<Recruiter> referents;

    @Autowired
    private JobServiceInterface jobServiceInterface;
    @Autowired
    private StudyLevelServiceInterface studyLevelServiceInterface;
    @Autowired
    private WorkingTimeServiceInterface workingTimeServiceInterface;
    @Autowired
    private ContractTypeServiceInterface contractTypeServiceInterface;
    @Autowired
    private RecruiterServiceInterface recruiterServiceInterface;
    @Autowired
    private StepServiceInterface stepServiceInterface;
    @Autowired
    private RecruitmentProcessServiceInterface recruitmentProcessServiceInterface;
    @Autowired
    private CandidateServiceInterface candidateServiceInterface;
    @Autowired
    private SecurityServiceManagement securityServiceManagement;


    //region Methods

    /**
     * Post construct method that initiates the situation depending on which type of profile is connected
     */
    @PostConstruct
    public void init() {
        jobs = jobServiceInterface.getAllJob();
        activeJobs = jobServiceInterface.getActiveJobs();

        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        if (session.getAttribute("connectedRecruiter") != null) {
            recruiter = (Recruiter) session.getAttribute("connectedRecruiter");
            userJobs = jobServiceInterface.getJobsByRecruiterId(recruiter.getId());
            referents = recruiterServiceInterface.GetAllByIdCompany(recruiter.getCompany().getId());
        }
        if (session.getAttribute("connectedCandidate") != null) {
            candidate = (Candidate) session.getAttribute("connectedCandidate");
            userJobs = jobServiceInterface.getJobsByCandidateId(candidate);
        }

        if (session.getAttribute("job") != null) job = (Job) session.getAttribute("job");

        times = workingTimeServiceInterface.getAllWorkingTime();
        levels = studyLevelServiceInterface.getAllStudyLevel();
        contracts = contractTypeServiceInterface.getAllContractType();
        radius = 0;
        setAllFilterDetails();
    }

    /**
     * Method for job's recruiter :
     * @return the list of Job link to the connected Recruiter.
     */
    public List<Job> currentJob() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        Recruiter recruiter = (Recruiter) session.getAttribute("connectedRecruiter");
        return jobServiceInterface.getJobsByRecruiterId(recruiter.getId());

    }
    /**
     * Method for job's candidate :
     * @return the list of Job link to the connected Candidate.
     */
    public List<Job> currentCandidateJob() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        Candidate candidate = (Candidate) session.getAttribute("connectedCandidate");
        return jobServiceInterface.getJobsByCandidateId(candidate);
    }

    /**
     * Method to check if there is a candidate recruited on the job in session
     * used for detailed recruiter dashboard to lock the change if a candidate is recruited.
     *
     * @return true if there is a candidate recruited on the job in session
     */
    public boolean aCandidateIsRecrutedOnCurrentJob() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        Job job = (Job) session.getAttribute("currentJob");
        if (jobServiceInterface.recrutedProcessOnThisJob(jobServiceInterface.getById(job.getId())).size() > 0)
            return true;
        else return false;
    }

    /**
     * Method show job details
     * Used in candidate dashbord to show job detail
     * set the job in session and open the job detail view.
     * @return the path to jobDetail
     */
    public String RedirectCandidateToDetailJob(long id) {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        session.setAttribute("currentJob", jobServiceInterface.getById(id));
        return "/jobDetail.xhtml?faces-redirection=true";
    }
    /**
     * Method to filter job in the jobSearch View
     * changes the job attribute according to the different criteria entered
     * @return the path to jobSearch
     */
    public String filterJob() {
        System.out.println("Start filtering job...");
        System.out.println("getting city from session ...");
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        city = session.getAttribute("city") != null ? (City) session.getAttribute("city") : new City();

        maxSalary = securityServiceManagement.isFormatSalaryIsOk(String.valueOf(maxSalary))?maxSalary:null;
        minSalary = securityServiceManagement.isFormatSalaryIsOk(String.valueOf(minSalary))?maxSalary:null;
        title = securityServiceManagement.removeSpecialChar(title);
        details = securityServiceManagement.removeSpecialChar(details);

        System.out.println("Checking input ...");
        System.out.println("min = :" + minSalary +
                "\n maxSalary :" + maxSalary +
                "\n level id : " + studyLevelId +
                "\n workingTimeId :" + workingTimeId +
                "\n city :" + city.getCityName() +
                "\n title :" + title +
                "\n radius :" + radius +
                "\n contractType :" + contractTypeId +
                "\n detail :" + details);

        System.out.println("Filtering job with inputs except city...");
        jobs = jobServiceInterface.filterJobs(minSalary, maxSalary, studyLevelId, workingTimeId, contractTypeId, title, details);

        if (city.getCityName() != null) {
            System.out.println("Filtering job with city ...");
            jobs = jobServiceInterface.filterJobsByCityAndRadius(jobs, city, radius);
        }
        System.out.println("Setting filter details ...");
        setAllFilterDetails();

        System.out.println("Cleaning attributes ...");
        cleanAttributes();

        System.out.println("Filtering job complete, return a list of " + jobs.size() + " jobs.");
        return "/jobSearch.xhtml?faces-redirection=true";
    }

    /**
     * Method to see how many candidate are still in process on the job
     *
     * @return number of candidate in process on the job (except 'sortie')
     */
    public int numberOfCandidateInProcess(long id) {
      int  result = 0;
        if (recruitmentProcessServiceInterface.getAllActiveByJobId(id) != null) {
            for (RecruitmentProcess r: recruitmentProcessServiceInterface.getAllActiveByJobId(id)) {
                if(r.getStep().getStepNumber() !=100) result ++;
            }
        }
        return result;
    }

    /**
     * Method to update the "filter" display attributes from the data entered to sort the jobs
     *
     */
    public void setAllFilterDetails() {
        filterMinSalary = minSalary == null ? "Pas de minimum" : String.valueOf(minSalary);
        filterMaxSalary = maxSalary == null ? "Pas de maximum" : String.valueOf(maxSalary);
        filterStudyLevel = studyLevelId == null ? "Sans" : studyLevelServiceInterface.getById(studyLevelId).getLevel();
        filterWorkingTime = workingTimeId == null ? "Sans" : workingTimeServiceInterface.getById(workingTimeId).getType();
        filterContractType = contractTypeId == null ? "Sans" : contractTypeServiceInterface.getById(contractTypeId).getType();
        filterTitle = title == null || title.equals("") ? "Aucun" : title;
        filterDetails = details == null || details.equals("") ? "Aucun" : details;
        filterCity = city == null || city.getCityName() == null ? "Toutes les villes" : city.getCityName() + " " + city.getPostCode();
        filterRadius = radius == 0 ? "0" : String.valueOf(radius);

        System.out.println("checking the filter value...");

        System.out.println(
                filterMinSalary +
                        filterMaxSalary +
                        filterStudyLevel +
                        filterWorkingTime +
                        filterContractType +
                        filterTitle +
                        filterDetails +
                        filterCity +
                        filterRadius
        );

    }

    /**
     * Method to clean attribute entered to filter job
     *
     */
    public void cleanAttributes() {
        minSalary = null;
        maxSalary = null;
        studyLevelId = null;
        workingTimeId = null;
        contractTypeId = null;
        title = null;
        details = null;
        city = null;
        radius = 0;
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        session.removeAttribute("city");
    }


    /**
     * Method know if the job given is active or not
     * @return true if job is active
     */
    public boolean isThatJobIsActive(long id){
     return jobServiceInterface.isActiveJob(jobServiceInterface.getById(id));
    }

    /**
     * Method know know the candidate hired on the job
     * @return the candidate or null if there is no candidate hired
     */
    public Candidate hiredCandidateOnTheJob(long id){

        return candidateServiceInterface.getCandidateHiredOnThisJob(jobServiceInterface.getById(id));

    }

    //endregion

    //region Accessors


    public List<Job> getActiveJobs() {
        return activeJobs;
    }

    public void setActiveJobs(List<Job> activeJobs) {
        this.activeJobs = activeJobs;
    }

    public List<Job> getJobs() {
        return jobs;
    }

    public void setJobs(List<Job> jobs) {
        this.jobs = jobs;
    }

    public List<Job> getUserJobs() {
        return userJobs;
    }

    public void setUserJobs(List<Job> userJobs) {
        this.userJobs = userJobs;
    }

    public Recruiter getRecruiter() {
        return recruiter;
    }

    public void setRecruiter(Recruiter recruiter) {
        this.recruiter = recruiter;
    }

    public Candidate getCandidate() {
        return candidate;
    }

    public void setCandidate(Candidate candidate) {
        this.candidate = candidate;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Integer getMaxSalary() {
        return maxSalary;
    }

    public void setMaxSalary(Integer maxSalary) {
        this.maxSalary = maxSalary;
    }

    public Integer getMinSalary() {
        return minSalary;
    }

    public void setMinSalary(Integer minSalary) {
        this.minSalary = minSalary;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDate getEffectiveHiringDate() {
        return effectiveHiringDate;
    }

    public void setEffectiveHiringDate(LocalDate effectiveHiringDate) {
        this.effectiveHiringDate = effectiveHiringDate;
    }

    public LocalDate getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(LocalDate closeDate) {
        this.closeDate = closeDate;
    }

    public StudyLevel getStudyLevel() {
        return studyLevel;
    }

    public void setStudyLevel(StudyLevel studyLevel) {
        this.studyLevel = studyLevel;
    }

    public ContractType getContractType() {
        return contractType;
    }

    public void setContractType(ContractType contractType) {
        this.contractType = contractType;
    }

    public WorkingTime getWorkingTime() {
        return workingTime;
    }

    public void setWorkingTime(WorkingTime workingTime) {
        this.workingTime = workingTime;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Long getContractTypeId() {
        return contractTypeId;
    }

    public void setContractTypeId(Long contractTypeId) {
        this.contractTypeId = contractTypeId;
    }

    public Long getWorkingTimeId() {
        return workingTimeId;
    }

    public void setWorkingTimeId(Long workingTimeId) {
        this.workingTimeId = workingTimeId;
    }

    public Long getStudyLevelId() {
        return studyLevelId;
    }

    public void setStudyLevelId(Long studyLevelId) {
        this.studyLevelId = studyLevelId;
    }

    public Long getReferentId() {
        return referentId;
    }

    public void setReferentId(Long referentId) {
        this.referentId = referentId;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public String getFilterMinSalary() {
        return filterMinSalary;
    }

    public void setFilterMinSalary(String filterMinSalary) {
        this.filterMinSalary = filterMinSalary;
    }

    public String getFilterMaxSalary() {
        return filterMaxSalary;
    }

    public void setFilterMaxSalary(String filterMaxSalary) {
        this.filterMaxSalary = filterMaxSalary;
    }

    public String getFilterStudyLevel() {
        return filterStudyLevel;
    }

    public void setFilterStudyLevel(String filterStudyLevel) {
        this.filterStudyLevel = filterStudyLevel;
    }

    public String getFilterWorkingTime() {
        return filterWorkingTime;
    }

    public void setFilterWorkingTime(String filterWorkingTime) {
        this.filterWorkingTime = filterWorkingTime;
    }

    public String getFilterContractType() {
        return filterContractType;
    }

    public void setFilterContractType(String filterContractType) {
        this.filterContractType = filterContractType;
    }

    public String getFilterTitle() {
        return filterTitle;
    }

    public void setFilterTitle(String filterTitle) {
        this.filterTitle = filterTitle;
    }

    public String getFilterDetails() {
        return filterDetails;
    }

    public void setFilterDetails(String filterDetails) {
        this.filterDetails = filterDetails;
    }

    public String getFilterCity() {
        return filterCity;
    }

    public void setFilterCity(String filterCity) {
        this.filterCity = filterCity;
    }

    public String getFilterRadius() {
        return filterRadius;
    }

    public void setFilterRadius(String filterRadius) {
        this.filterRadius = filterRadius;
    }

    public List<WorkingTime> getTimes() {
        return times;
    }

    public void setTimes(List<WorkingTime> times) {
        this.times = times;
    }

    public List<StudyLevel> getLevels() {
        return levels;
    }

    public void setLevels(List<StudyLevel> levels) {
        this.levels = levels;
    }

    public List<ContractType> getContracts() {
        return contracts;
    }

    public void setContracts(List<ContractType> contracts) {
        this.contracts = contracts;
    }

    public List<Recruiter> getReferents() {
        return referents;
    }

    public void setReferents(List<Recruiter> referents) {
        this.referents = referents;
    }


//endregion
}
