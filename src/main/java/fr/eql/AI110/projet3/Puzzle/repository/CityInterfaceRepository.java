package fr.eql.AI110.projet3.Puzzle.repository;

import fr.eql.AI110.projet3.Puzzle.model.City;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CityInterfaceRepository extends JpaRepository<City, Long> {

    City findByPostCodeAndCityName(String postCode, String cityName);
}
