/**
 * This controller is a REST controller for JSON and String communication with the Javascript part of the application
 * It gives a lot of asynchronous possibility for different functionality linked to job management
 *
 * @version 1.0
 *
 * @see fr.eql.AI110.projet3.Puzzle.controller.JobManagedBean
 * @author Jean-Bernard HEIL
 */

package fr.eql.AI110.projet3.Puzzle.controller;


import fr.eql.AI110.projet3.Puzzle.model.RecruitmentProcess;
import fr.eql.AI110.projet3.Puzzle.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.time.LocalDate;

@RestController
@RequestMapping("/actionToJob")
public class JobRestController {

    @Autowired
    StepServiceInterface stepServiceInterface;
    @Autowired
    private JobServiceInterface jobServiceInterface;
    @Autowired
    private StudyLevelServiceInterface studyLevelServiceInterface;
    @Autowired
    private WorkingTimeServiceInterface workingTimeServiceInterface;
    @Autowired
    private ContractTypeServiceInterface contractTypeServiceInterface;
    @Autowired
    private RecruiterServiceInterface recruiterServiceInterface;
    @Autowired
    private RecruitmentProcessServiceInterface recruitmentProcessServiceInterface;
    @Autowired
    private CandidateServiceInterface candidateServiceInterface;
    @Autowired
    private ResumeServiceInterface resumeServiceInterface;


    /**
     * Get method to propose a job to a candidate
     * @return a confirmation or a alert write in a string
     */
    @GetMapping("/propose")
    public String proposeJob(
            HttpSession session,
            @RequestParam String candidateId,
            @RequestParam String jobId
    ) {
        if(session.getAttribute("connectedRecruiter")!=null){

        System.out.println("Job is : " + jobId + " Candidate is : " + candidateId);
        String result = "";
        if (recruitmentProcessServiceInterface.IsThatCandidateIsOnThisJob(Long.parseLong(candidateId),Long.parseLong(jobId))==false) {
            System.out.println("Candidate isn't on the job");
            recruitmentProcessServiceInterface.createProcess(new RecruitmentProcess(null, LocalDate.now(), null, null, "poste proposé par le recruteur",
                    candidateServiceInterface.getById(Long.parseLong(candidateId)),
                    stepServiceInterface.GetStepByJobIdAndStepNumber(Long.parseLong(jobId), 1),
                    resumeServiceInterface.getAllResumeByIdCandidate(Long.parseLong(candidateId)).get(0)));

            result = candidateServiceInterface.getById(Long.parseLong(candidateId)).getFirstName() +" "+
                    candidateServiceInterface.getById(Long.parseLong(candidateId)).getLastName() + " à été ajouté sur le poste " + jobServiceInterface.getById(Long.parseLong(jobId)).getTitle();
        }else {
            System.out.println("Candidate already on the job");
            result = "Le candidat a déjà un process en cours sur ce poste.";
        }

        return result;
        }else return null;
    }

    /**
     * Method to set a job in session.
     */
    @GetMapping("/setJob")
    public void SetJobToAttribute(
            HttpSession session,
            @RequestParam String jobId){
        session.setAttribute("currentJob",jobServiceInterface.getById(Long.parseLong(jobId)));
    }
}
