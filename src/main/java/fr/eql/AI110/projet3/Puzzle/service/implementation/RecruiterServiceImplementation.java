package fr.eql.AI110.projet3.Puzzle.service.implementation;

import fr.eql.AI110.projet3.Puzzle.model.Job;
import fr.eql.AI110.projet3.Puzzle.model.Recruiter;
import fr.eql.AI110.projet3.Puzzle.repository.RecruiterInterfaceRepository;
import fr.eql.AI110.projet3.Puzzle.service.RecruiterServiceInterface;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.NoSuchAlgorithmException;
import java.util.List;


@Service
@Slf4j
public class RecruiterServiceImplementation implements RecruiterServiceInterface {
    @Autowired
    private RecruiterInterfaceRepository recruiterInterfaceRepository;
    @Autowired
    private LoginServiceImplementation loginServiceImplementation;

    @Override
    public Recruiter create(Recruiter recruiter) {
        log.info("Saving new recruiter : {} {}", recruiter.getFirstName(), recruiter.getLastName());
        return recruiterInterfaceRepository.save(recruiter);
    }

    @Override
    public Recruiter connect(String mail, String password) throws NoSuchAlgorithmException {
        log.info("Trying to connect as recruiter : {} ", mail);

        String hashedPassword = loginServiceImplementation.hashAndSaltPassword(password, mail);
        System.out.println(hashedPassword);

        Recruiter recruiter = recruiterInterfaceRepository.findByMailAndHashedPassword(mail, hashedPassword);

        if (recruiter!=null){
            log.info("Success to connect as recruiter : {} ", mail);
            return recruiter;
        } else {
            log.error("Fail to connect as recruiter : {} ", mail);
            return null;
        }
    }

    @Override
    public List<Recruiter> GetAllByIdCompany(long id) {
        return recruiterInterfaceRepository.findAllByCompanyId(id);
    }

    @Override
    public Recruiter getById(long id) {
       return recruiterInterfaceRepository.findById(id);
    }

    @Override
    public Recruiter getByJob(Job job) {
        return recruiterInterfaceRepository.findByJob(job);
    }
}
