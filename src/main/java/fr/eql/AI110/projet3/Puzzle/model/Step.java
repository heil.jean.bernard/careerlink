/**
 * This class represent a Step.
 * A step composed a Job
 * There are mandatory step and custom step (define by recruiter)
 *
 * @version 1.0
 *
 * @see fr.eql.AI110.projet3.Puzzle.model.Job
 * @see fr.eql.AI110.projet3.Puzzle.model.StepType
 * @see fr.eql.AI110.projet3.Puzzle.model.Recruiter
 * @author Jean-Bernard HEIL
 */
package fr.eql.AI110.projet3.Puzzle.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.List;

@JsonIgnoreProperties({ "job", "recruitmentProcessSet" })
@Entity
@Table(name = "step")
public class Step implements Comparable<Step> {
    //region Properties
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "referentfirstname")
    private String referentFirstName;
    @Column(name = "referentlastname")
    private String referentlastName;
    @Column(name = "details", columnDefinition="LONGTEXT")
    private String details;
    @Column(name = "referentmail")
    private String referentMail;
    @Column(name = "name")
    private String name;
    @Column(name = "stepNumber")
    private int stepNumber;
    //endregion

    //region Relational
    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private Job job;
    @OneToMany(mappedBy = "step", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<RecruitmentProcess> recruitmentProcessSet;
    //endregion

    //region Methods

    //endregion

    //region Accessors
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReferentFirstName() {
        return referentFirstName;
    }

    public void setReferentFirstName(String referentFirstName) {
        this.referentFirstName = referentFirstName;
    }

    public String getReferentlastName() {
        return referentlastName;
    }

    public void setReferentlastName(String referentlastName) {
        this.referentlastName = referentlastName;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getReferentMail() {
        return referentMail;
    }

    public void setReferentMail(String referentMail) {
        this.referentMail = referentMail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStepNumber() {
        return stepNumber;
    }

    public void setStepNumber(int stepNumber) {
        this.stepNumber = stepNumber;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public List<RecruitmentProcess> getrecruitmentProcessSet() {
        return recruitmentProcessSet;
    }

    public void setRecruitmentProcessSet(List<RecruitmentProcess> recruitmentProcessSet) {
        this.recruitmentProcessSet = recruitmentProcessSet;
    }

    //endregion

    //region Constructors
    public Step(Long id, String referentFirstName, String referentlastName, String details, String referentMail, String name, int stepNumber, Job job, List<RecruitmentProcess> recruitmentProcessSet) {
        this.id = id;
        this.referentFirstName = referentFirstName;
        this.referentlastName = referentlastName;
        this.details = details;
        this.referentMail = referentMail;
        this.name = name;
        this.stepNumber = stepNumber;
        this.job = job;
        this.recruitmentProcessSet = recruitmentProcessSet;
    }
    public Step() {
    }

    @Override
    public int compareTo(Step o) {
        return (this.getStepNumber()-o.getStepNumber());
    }
    //endregion
}