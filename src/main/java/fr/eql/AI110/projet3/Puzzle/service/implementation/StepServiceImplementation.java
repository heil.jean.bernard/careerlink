package fr.eql.AI110.projet3.Puzzle.service.implementation;

import fr.eql.AI110.projet3.Puzzle.model.Job;
import fr.eql.AI110.projet3.Puzzle.model.Step;
import fr.eql.AI110.projet3.Puzzle.repository.RecruitmentProcessRepository;
import fr.eql.AI110.projet3.Puzzle.repository.StepInterfaceRepository;
import fr.eql.AI110.projet3.Puzzle.service.JobServiceInterface;
import fr.eql.AI110.projet3.Puzzle.service.StepServiceInterface;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Slf4j
@Service
public class StepServiceImplementation implements StepServiceInterface {

    @Autowired
    private StepInterfaceRepository stepInterfaceRepository;
    @Autowired
    private JobServiceInterface jobServiceInterface;
    @Autowired
    private RecruitmentProcessRepository recruitmentProcessRepository;


    @Override
    public List<Step> getAllStep() {
       return stepInterfaceRepository.findAll();
    }

    @Override
    public List<Step> getAllByJobId(long id) {
        List<Step> allSteps = stepInterfaceRepository.findAllByJobId(id);
        Collections.sort(allSteps);
        return allSteps;
    }

    @Override
    public Step saveStep(Step step) {
        return stepInterfaceRepository.save(step);
    }

    @Override
    public Step getStepById(long id) {
        return stepInterfaceRepository.findById(id);
    }

    @Override
    public Step GetStepByJobIdAndStepNumber(long id, int number) {
        return stepInterfaceRepository.findByJobIdAndStepNumber(id, number);
    }


    @Override
    public List<Step> finishedStep(long idCandidate, long idJob) {
        List<Step> allSteps = stepInterfaceRepository.findAllByJobId(idJob);
        List<Step> finishedSteps = new ArrayList<Step>();

        if (actualStep(idCandidate,idJob).getStepNumber()==0){

        }

        for (Step step:allSteps) {
            if (recruitmentProcessRepository.findByCandidateIdAndStepIdAndOkDateNotNull(idCandidate, step.getId())!=null) {
                finishedSteps.add(step);
            }
        }

        Collections.sort(finishedSteps);
        return finishedSteps;
    }

    @Override
    public Step actualStep(long idCandidate, long idJob) {
        List<Step> allSteps = stepInterfaceRepository.findAllByJobId(idJob);
        Step result = new Step();
        result.setName("erreur");

        for (Step step:allSteps) {
            if (recruitmentProcessRepository.findByCandidateIdAndStepIdAndNotDateIsNullAndOkDateIsNull(idCandidate,step.getId())!=null) {
                result=getStepById(step.getId());
                break;
            }
        }
        return result;
    }

    @Override
    public List<Step> todoStep(long idCandidate, long idJob) {
        List<Step> allSteps = stepInterfaceRepository.findAllByJobId(idJob);
        List<Step> toDodSteps = new ArrayList<Step>();

        for (Step step:allSteps) {
            if (step.getStepNumber()!=100 && step.getStepNumber()!=20 && step.getStepNumber()!=1 && recruitmentProcessRepository.findByCandidateIdAndStepIdAndOkDateNotNull(idCandidate, step.getId())== null
            && recruitmentProcessRepository.findByCandidateIdAndStepIdAndNotDateIsNullAndOkDateIsNull(idCandidate,step.getId()) == null
            ) {
                toDodSteps.add(step);
            }
        }
        Collections.sort(toDodSteps);
        return toDodSteps;
    }

    @Override
    public Step getNextStep(long jobId, Step step) {

        List<Step> steps = getAllByJobId(jobId);
        List<Long> stepsId=new ArrayList<Long>();
        for (Step stepId :
                steps) {
            stepsId.add(stepId.getId());
        }

        int index = stepsId.indexOf(step.getId());
        return steps.get(index+1);
    }

    @Override
    public int getPosition(long jobId, Step step) {

        List<Step> steps = getAllByJobId(jobId);
        List<Long> stepsId=new ArrayList<Long>();
        for (Step stepId :
                steps) {
            stepsId.add(stepId.getId());
            System.out.println(stepId.getId() +" "+stepId.getStepNumber());
        }

       return stepsId.indexOf(step.getId());
    }

    public boolean addStepToJob(Job jobToAddStep, String stepName, String firstName, String lastName, String detail, String mail){
        log.info("start adding step : {} to job : {}",stepName,jobToAddStep.getTitle());
        log.debug("checking if job is not null");
        if (jobToAddStep != null) {
            log.debug("job isn't null, create new step");
            saveStep(new Step(null,firstName,lastName,detail,mail,stepName,getAllByJobId(jobToAddStep.getId()).size()-2,jobToAddStep,null));
            log.debug("saving step");
            return true;
        }else {
            log.info("add failed, Job is null");
            return false;
        }
    }

    public List<Step> getStepListWithoutStep1AndStep100(Job job){
        List<Step> steps = stepInterfaceRepository.findAllStepWithoutStep1AndStep100ByJob(job);
       Collections.sort(steps);
       return steps;
    }

}

