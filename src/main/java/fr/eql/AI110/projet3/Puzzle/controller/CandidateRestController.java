/**
 * This controller is a REST controller for JSON and String communication with the Javascript part of the application
 * It gives a lot of asynchronous possibility for different functionality
 *
 * @version 1.0
 *
 * @see fr.eql.AI110.projet3.Puzzle.controller.CandidateManagedBean
 * @author Jean-Bernard HEIL
 */


package fr.eql.AI110.projet3.Puzzle.controller;


import fr.eql.AI110.projet3.Puzzle.model.*;
import fr.eql.AI110.projet3.Puzzle.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;

@RestController
@RequestMapping("/candidate")
public class CandidateRestController {

    @Autowired
    StepServiceInterface stepServiceInterface;
    @Autowired
    private StudyLevelServiceInterface studyLevelServiceInterface;
    @Autowired
    private LoginServiceInterface loginServiceInterface;
    @Autowired
    private AvailabilityServiceInterface availabilityServiceInterface;
    @Autowired
    private CandidateServiceInterface candidateServiceInterface;
    @Autowired
    private RecruitmentProcessServiceInterface recruitmentProcessServiceInterface;
    @Autowired
    private JobServiceInterface jobServiceInterface;

    @Autowired
    private ResumeServiceInterface resumeServiceInterface;

    @Autowired
    ServletContext context;

    @GetMapping("/checkIfExistAlready")
    public String checkIfMailExist(@RequestParam String mail) {

        if (candidateServiceInterface.getByMail(mail) != null) return "non dispo";
        return null;
    }

    @PostMapping("/createAccount")
    public String createJob(
            HttpSession session,
            @RequestParam String firstName,
            @RequestParam String lastName,
            @RequestParam String password,
            @RequestParam String mail,
            @RequestParam String phone,
            @RequestParam String period,
            @RequestParam String level,
            @RequestParam String salary,
            @RequestParam String visible,
            @RequestParam MultipartFile resume) throws IOException, NoSuchAlgorithmException {

        //Get city from session (add with SetCityInfo method)
        System.out.println("Get city from session.....");
        City city = (City) session.getAttribute("city");

        //Save the resume in resume file via MultipartFile (use context to get the good path)
        System.out.println("Save resume in resume directory.........");
        resume.transferTo(new File(context.getRealPath("resources/resume/") + resume.getOriginalFilename()));

        //hash password
        System.out.println("Hashing pasword.....");
        String hashedPassword = loginServiceInterface.hashAndSaltPassword(password, mail);


//add candidate in database
        System.out.println("Add candidate.....");
        Candidate candidate = candidateServiceInterface.create(
                new Candidate(
                        null,
                        firstName,
                        lastName,
                        mail,
                        hashedPassword,
                        phone,
                        Integer.parseInt(salary),
                        LocalDate.now(),
                        city,
                        availabilityServiceInterface.getById(Long.parseLong(period)),
                        studyLevelServiceInterface.getById(Long.parseLong(level)),
                        null,
                        null,
                        Boolean.parseBoolean(visible)));

        //add resume link to candidate in database
        System.out.println("Add resume and link to candidate.....");
        resumeServiceInterface.createResume(new Resume(
                null,
                resume.getOriginalFilename(),
                candidate,
                null));

//remove city from session
        System.out.println("Remove city from session.....");
        session.removeAttribute("city");


        return "Le compte pour le candidat " + candidate.getFirstName() + " " + candidate.getLastName() + " à bien été créé";
    }


    @GetMapping("/checkIfCandidateOnJob")
    public String checkIfCandidateonJob(@RequestParam String candidateId, @RequestParam String jobId) {

        if (recruitmentProcessServiceInterface.IsThatCandidateIsOnThisJob(Long.parseLong(candidateId), Long.parseLong(jobId)) == false)
            return null;
        else return "Déjà sur le poste";

    }

    @PostMapping("/applyToJob")
    public String applyToJob(@RequestParam String candidateId, @RequestParam String jobId) {

        recruitmentProcessServiceInterface.createProcess(new RecruitmentProcess(
                null,
                LocalDate.now(),
                null,
                null,
                "Le candidat à postulé via la recherche de poste",
                candidateServiceInterface.getById(Long.parseLong(candidateId)),
                stepServiceInterface.GetStepByJobIdAndStepNumber(Long.parseLong(jobId), 2),
                resumeServiceInterface.getAllResumeByIdCandidate(Long.parseLong(candidateId)).get(0)));

        return "Vous avez postulé pour le poste : "+jobServiceInterface.getById(Long.parseLong(jobId)).getTitle();
    }
}
