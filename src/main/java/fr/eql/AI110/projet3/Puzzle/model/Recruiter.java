/**
 * This class represent a recruiter in the application
 * It has multiple connexion with other class of the application
 *
 * @version 1.0
 *
 * @see fr.eql.AI110.projet3.Puzzle.model.City
 * @see fr.eql.AI110.projet3.Puzzle.model.Job
 * @see fr.eql.AI110.projet3.Puzzle.model.Company

 * @author Jean-Bernard HEIL
 */

package fr.eql.AI110.projet3.Puzzle.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Table (name = "recruiter")
public class Recruiter {
    //region Properties
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "firstname")
    private String firstName;
    @Column(name = "lastname")
    private String lastName;
    @Column(name = "mail")
    private String mail;
    @Column(name = "photo")
    private String photo;
    @Column(name = "hashedpassword")
    private String hashedPassword;
    @Column(name = "creationdate")
    private LocalDate creationDate;
    //endregion

    //region Relational
    @OneToMany(mappedBy = "recruiter", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Job> jobSet;
    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private City city;
    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private Company company;

    //endregion

    //region Methods

    //endregion

    //region Accessors
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getHashedPassword() {
        return hashedPassword;
    }

    public void setHashedPassword(String hashedPassword) {
        this.hashedPassword = hashedPassword;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public Set<Job> getJobSet() {
        return jobSet;
    }

    public void setJobSet(Set<Job> jobSet) {
        this.jobSet = jobSet;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
    //endregion

    //region Constructors
    public Recruiter(Long id, String firstName, String lastName, String mail, String photo, String hashedPassword, LocalDate creationDate, Set<Job> jobSet, City city, Company company) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.mail = mail;
        this.photo = photo;
        this.hashedPassword = hashedPassword;
        this.creationDate = creationDate;
        this.jobSet = jobSet;
        this.city = city;
        this.company = company;
    }

    public Recruiter() {
    }
    //endregion
}