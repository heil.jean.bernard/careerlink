/**
 * This class is a link to a candidate's resume (pdf file) upload by candidate and stocked locally
 * A candidate can be linked with multiple resume, but one process have one resume
 *
 * @version 1.0
 *
 * @see fr.eql.AI110.projet3.Puzzle.model.Candidate
 * @see fr.eql.AI110.projet3.Puzzle.model.RecruitmentProcess
 * @author Jean-Bernard HEIL
 */

package fr.eql.AI110.projet3.Puzzle.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table (name = "resume")
public class Resume {
    //region Properties
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "path")
    private String path;
    //endregion

    //region Relational
    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private Candidate candidate;
    @OneToMany(mappedBy = "resume", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<RecruitmentProcess> recruitmentProcessSet;
    //endregion

    //region Methods

    //endregion

    //region Accessors
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Candidate getCandidate() {
        return candidate;
    }

    public void setCandidate(Candidate candidate) {
        this.candidate = candidate;
    }

    public Set<RecruitmentProcess> getRecruitmentProcessSet() {
        return recruitmentProcessSet;
    }

    public void setRecruitmentProcessSet(Set<RecruitmentProcess> recruitmentProcessSet) {
        this.recruitmentProcessSet = recruitmentProcessSet;
    }
    //endregion

    //region Constructors
    public Resume() {
    }

    public Resume(Long id, String path, Candidate candidate, Set<RecruitmentProcess> recruitmentProcessSet) {
        this.id = id;
        this.path = path;
        this.candidate = candidate;
        this.recruitmentProcessSet = recruitmentProcessSet;
    }
    //endregion
}
