package fr.eql.AI110.projet3.Puzzle.service;

import fr.eql.AI110.projet3.Puzzle.model.Job;
import fr.eql.AI110.projet3.Puzzle.model.RecruitmentProcess;
import fr.eql.AI110.projet3.Puzzle.model.Step;

import java.util.List;

public interface RecruitmentProcessServiceInterface {

    List<RecruitmentProcess> getAllProcessByStepId(long id);
    RecruitmentProcess createProcess (RecruitmentProcess recruitmentProcess);
    List<RecruitmentProcess> getAllActiveByStepId(long id);
    List<RecruitmentProcess> getAllActiveByJobId(long id);
    List<RecruitmentProcess> getAllByCandidateId(long id);
    boolean IsThatCandidateIsOnThisJob(long idCandidate, long idJob);
    RecruitmentProcess actualProcess(long candidateId, long jobId);

}
