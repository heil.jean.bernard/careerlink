/**
 * This class represent a company in the application
 * Company in the application is just a part of a recruiter description
 * but it's possible to have many recruiters linked to a unique company
 * It's also usefully to get Job grouped by company through recruiters
 *
 * @version 1.0
 *
 * @see fr.eql.AI110.projet3.Puzzle.model.City
 * @see fr.eql.AI110.projet3.Puzzle.model.Recruiter

 * @author Jean-Bernard HEIL
 */

package fr.eql.AI110.projet3.Puzzle.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table ( name = "company")
public class Company {
    //region Properties
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "siret")
    private String siret;
    @Column(name = "hashedpassword")
    private String hashedPassword;
    @Column(name = "logo")
    private String logo;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    //endregion

    //region Relational
    @OneToMany(mappedBy = "company", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Recruiter> recruiterSet;
    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private City city;
    //endregion

    //region Constructors
    public Company() {
    }
    public Company(Long id, City city, String name, String description, Set<Recruiter> recruiterSet, String siret, String hashedPassword, String logo) {
        this.id = id;
        this.city = city;
        this.name = name;
        this.description = description;
        this.recruiterSet = recruiterSet;
        this.siret = siret;
        this.hashedPassword = hashedPassword;
        this.logo = logo;
    }
    //endregion

    //region Accessors
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Recruiter> getRecruiterSet() {
        return recruiterSet;
    }

    public void setRecruiterSet(Set<Recruiter> recruiterSet) {
        this.recruiterSet = recruiterSet;
    }

    public String getSiret() {
        return siret;
    }

    public void setSiret(String siret) {
        this.siret = siret;
    }

    public String getHashedPassword() {
        return hashedPassword;
    }

    public void setHashedPassword(String hashedPassword) {
        this.hashedPassword = hashedPassword;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
    //endregion

    //region Methods

    //endregion
}