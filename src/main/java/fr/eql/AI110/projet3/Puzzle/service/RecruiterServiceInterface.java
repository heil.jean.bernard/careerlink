package fr.eql.AI110.projet3.Puzzle.service;

import fr.eql.AI110.projet3.Puzzle.model.Job;
import fr.eql.AI110.projet3.Puzzle.model.Recruiter;

import java.security.NoSuchAlgorithmException;
import java.util.List;

public interface RecruiterServiceInterface {
    Recruiter create(Recruiter recruiter);
    Recruiter connect(String mail, String password) throws NoSuchAlgorithmException;
    List<Recruiter> GetAllByIdCompany(long id);
    Recruiter getById(long id);
    Recruiter getByJob(Job job);
}
