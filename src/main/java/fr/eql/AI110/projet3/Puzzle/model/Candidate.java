/**
 * This class represent a candidate in the application
 * It has multiple connexion with other class of the application
 *
 * @version 1.0
 *
 * @see fr.eql.AI110.projet3.Puzzle.model.City
 * @see fr.eql.AI110.projet3.Puzzle.model.AvailabilityPeriod
 * @see fr.eql.AI110.projet3.Puzzle.model.StudyLevel
 * @see fr.eql.AI110.projet3.Puzzle.model.RecruitmentProcess
 * @see fr.eql.AI110.projet3.Puzzle.model.Resume
 * @author Jean-Bernard HEIL
 */

package fr.eql.AI110.projet3.Puzzle.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Entity
@Table (name = "candidate")
public class Candidate {
    //region Properties
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "firstname")
    private String firstName;
    @Column(name = "lastname")
    private String lastName;
    @Column(name = "mail")
    private String mail;
    @Column(name = "hashedpassword")
    private String hashedPassword;
    @Column(name = "phonenumber")
    private String phoneNumber;
    @Column(name = "salary")
    private int salary;
    @Column(name = "creationdate")
    private LocalDate creationDate;
    @Column(name = "visible")
    private boolean visible;
    //endregion

    //region Relational
    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private City city;
    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private AvailabilityPeriod availabilityPeriod;
    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private StudyLevel studyLevel;
    @OneToMany(mappedBy = "candidate", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<RecruitmentProcess> processSet;
    @OneToMany(mappedBy = "candidate", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Resume> resumeSet;
    //endregion

    //region Constructors
    public Candidate() {
    }
    public Candidate(Long id, String firstName, String lastName, String mail, String hashedPassword, String phoneNumber, int salary, LocalDate creationDate, City city, AvailabilityPeriod availabilityPeriod, StudyLevel studyLevel, Set<RecruitmentProcess> processSet, List<Resume> resumeSet,boolean visible) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.mail = mail;
        this.hashedPassword = hashedPassword;
        this.phoneNumber = phoneNumber;
        this.salary = salary;
        this.creationDate = creationDate;
        this.city = city;
        this.availabilityPeriod = availabilityPeriod;
        this.studyLevel = studyLevel;
        this.processSet = processSet;
        this.resumeSet = resumeSet;
        this.visible = visible;
    }

    public Candidate(Long id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Candidate(Long id, String firstName, String lastName, String mail, String hashedPassword, String phoneNumber, int salary, LocalDate creationDate, boolean visible, City city, AvailabilityPeriod availabilityPeriod, StudyLevel studyLevel, Set<RecruitmentProcess> processSet, List<Resume> resumeSet) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.mail = mail;
        this.hashedPassword = hashedPassword;
        this.phoneNumber = phoneNumber;
        this.salary = salary;
        this.creationDate = creationDate;
        this.visible = visible;
        this.city = city;
        this.availabilityPeriod = availabilityPeriod;
        this.studyLevel = studyLevel;
        this.processSet = processSet;
        this.resumeSet = resumeSet;
    }

    //endregion

    //region Methods

    //endregionss

    //region Accessors
    public Long getId() {
        return id;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getHashedPassword() {
        return hashedPassword;
    }

    public void setHashedPassword(String hashedPassword) {
        this.hashedPassword = hashedPassword;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public AvailabilityPeriod getAvailabilityPeriod() {
        return availabilityPeriod;
    }

    public void setAvailabilityPeriod(AvailabilityPeriod availabilityPeriod) {
        this.availabilityPeriod = availabilityPeriod;
    }

    public StudyLevel getStudyLevel() {
        return studyLevel;
    }

    public void setStudyLevel(StudyLevel studyLevel) {
        this.studyLevel = studyLevel;
    }

    public Set<RecruitmentProcess> getProcessSet() {
        return processSet;
    }

    public void setProcessSet(Set<RecruitmentProcess> processSet) {
        this.processSet = processSet;
    }

    public List<Resume> getResumeSet() {
        return resumeSet;
    }

    public void setResumeSet(List<Resume> resumeSet) {
        this.resumeSet = resumeSet;
    }
    //endregion

}
