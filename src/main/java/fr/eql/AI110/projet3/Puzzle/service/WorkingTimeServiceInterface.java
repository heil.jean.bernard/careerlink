package fr.eql.AI110.projet3.Puzzle.service;

import fr.eql.AI110.projet3.Puzzle.model.WorkingTime;

import java.util.List;

public interface WorkingTimeServiceInterface {

    List<WorkingTime> getAllWorkingTime();
    WorkingTime getByType(String type);
    WorkingTime getById(long id);
}
