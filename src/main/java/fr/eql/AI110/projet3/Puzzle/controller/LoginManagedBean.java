/**
 * This controller is related to log in management and connection.
 *
 * @version 1.0
 *
 * @author Jean-Bernard HEIL
 */

package fr.eql.AI110.projet3.Puzzle.controller;

import fr.eql.AI110.projet3.Puzzle.model.Candidate;
import fr.eql.AI110.projet3.Puzzle.model.Recruiter;
import fr.eql.AI110.projet3.Puzzle.service.CandidateServiceInterface;
import fr.eql.AI110.projet3.Puzzle.service.RecruiterServiceInterface;
import fr.eql.AI110.projet3.Puzzle.service.SecurityServiceManagement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;

@Controller(value = "mbLogin")
@Scope(value = "session")
public class LoginManagedBean implements Serializable {

    @Autowired
    private CandidateServiceInterface candidateServiceInterface;
    @Autowired
    private RecruiterServiceInterface recruiterServiceInterface;
    @Autowired
    private SecurityServiceManagement securityServiceManagement;

    private String mail;
    private String password;
    private Candidate sessionCandidate;
    private Recruiter sessionRecruiter;

    private static final long serialVersionUID = 1L;

    //region Methods

    /**
     * Method to disconnect actual user Candidate or Recruiter
     * @return to the index page
     */
    public String disconnect() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        session.invalidate();
        return "/index.xhtml?faces-redirection=false";
    }

    /**
     * Method to connect a  user, Candidate or Recruiter
     * The method tried to connect as candidate first, then as recruiter.
     * @return to the index page
     */
    public String connect() throws NoSuchAlgorithmException {

        if (securityServiceManagement.isMailFormatOk(mail)) {
            sessionCandidate = candidateServiceInterface.connect(mail, password);
            sessionRecruiter = recruiterServiceInterface.connect(mail, password);
        }

        if (sessionCandidate != null) {
            System.out.println("Connect ok");
            HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
            session.setAttribute("connectedCandidate", sessionCandidate);

            return "/index.xhtml?faces-redirection=true";
        } else if (sessionRecruiter != null) {
            System.out.println("Connect ok");
            HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
            session.setAttribute("connectedRecruiter", sessionRecruiter);
            return "/index.xhtml?faces-redirection=true";

        } else {
            System.out.println("Impossible to connect !");
            FacesMessage facesMessage = new FacesMessage(
                    FacesMessage.SEVERITY_WARN,
                    "Identifiant et/ou mot de passe incorrect(s)",
                    "Identifiant et/ou mot de passe incorrect(s)"
            );
            FacesContext.getCurrentInstance().addMessage("loginForm:inpMail", facesMessage);
            FacesContext.getCurrentInstance().addMessage("loginForm:inpPassword", facesMessage);

            return "/Login.xhtml?faces-redirection=false";
        }
    }

    //endregion

    //region Accessors

    public CandidateServiceInterface getCandidateServiceInterface() {
        return candidateServiceInterface;
    }

    public void setCandidateServiceInterface(CandidateServiceInterface candidateServiceInterface) {
        this.candidateServiceInterface = candidateServiceInterface;
    }

    public RecruiterServiceInterface getRecruiterServiceInterface() {
        return recruiterServiceInterface;
    }

    public void setRecruiterServiceInterface(RecruiterServiceInterface recruiterServiceInterface) {
        this.recruiterServiceInterface = recruiterServiceInterface;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Candidate getSessionCandidate() {
        return sessionCandidate;
    }

    public void setSessionCandidate(Candidate sessionCandidate) {
        this.sessionCandidate = sessionCandidate;
    }

    public Recruiter getSessionRecruiter() {
        return sessionRecruiter;
    }

    public void setSessionRecruiter(Recruiter sessionRecruiter) {
        this.sessionRecruiter = sessionRecruiter;
    }


    //endregion


}
