/**
 * This class is an association class between class Step and class Candidate
 * It represents the evolution of the recruitment process, linked with a candidate
 *
 * @version 1.0
 *
 * @see fr.eql.AI110.projet3.Puzzle.model.Candidate
 * @see fr.eql.AI110.projet3.Puzzle.model.Step
 * @see fr.eql.AI110.projet3.Puzzle.model.Resume

 * @author Jean-Bernard HEIL
 */

package fr.eql.AI110.projet3.Puzzle.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "recruitmentprocess")
public class RecruitmentProcess {
    //region Properties
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "startdate")
    private LocalDate startDate;
    @Column(name = "okdate")
    private LocalDate okDate;
    @Column(name = "notokdate")
    private LocalDate notDate;
    @Column(name = "details", columnDefinition="LONGTEXT")
    private String details;
    //endregion

    //region Relational
    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private Candidate candidate;
    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private Step step;
    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private Resume resume;
    //endregion

    //region Constructors
    public RecruitmentProcess() {
    }
    public RecruitmentProcess(Long id, LocalDate startDate, LocalDate okDate, LocalDate notDate, String details, Candidate candidate, Step step, Resume resume) {
        this.id = id;
        this.startDate = startDate;
        this.okDate = okDate;
        this.notDate = notDate;
        this.details = details;
        this.candidate = candidate;
        this.step = step;
        this.resume = resume;
    }
    //endregion

    //region Methods

    //endregion

    //region Accessors
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getOkDate() {
        return okDate;
    }

    public void setOkDate(LocalDate okDate) {
        this.okDate = okDate;
    }

    public LocalDate getNotDate() {
        return notDate;
    }

    public void setNotDate(LocalDate notDate) {
        this.notDate = notDate;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Candidate getCandidate() {
        return candidate;
    }

    public void setCandidate(Candidate candidate) {
        this.candidate = candidate;
    }

    public Step getStep() {
        return step;
    }

    public void setStep(Step step) {
        this.step = step;
    }

    public Resume getResume() {
        return resume;
    }

    public void setResume(Resume resume) {
        this.resume = resume;
    }
    //endregion


}
