package fr.eql.AI110.projet3.Puzzle.repository;

import fr.eql.AI110.projet3.Puzzle.model.RecruitmentProcess;
import fr.eql.AI110.projet3.Puzzle.model.Step;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RecruitmentProcessRepository extends JpaRepository<RecruitmentProcess, Long> {

   List<RecruitmentProcess> findByStepId(long id);
   List<RecruitmentProcess> findByCandidateId(long id);

   //to search step with Ok date, finished step
   RecruitmentProcess findByCandidateIdAndStepIdAndOkDateNotNull(long candidateId, long stepId);

   //to search actual step : noOkDate or NotOkDate
   RecruitmentProcess findByCandidateIdAndStepIdAndNotDateIsNullAndOkDateIsNull(long candidateId, long stepId);

   List<RecruitmentProcess> findByStepIdAndNotDateIsNullAndOkDateIsNull(long id);

   List<RecruitmentProcess> findByStep(Step step);
   //To know if candidate as already process on the Job
   List<RecruitmentProcess> findByCandidateIdAndStepId(long idCandidate, long idStep);
}
