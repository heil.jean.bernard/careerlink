package fr.eql.AI110.projet3.Puzzle.service;

import fr.eql.AI110.projet3.Puzzle.model.Candidate;
import fr.eql.AI110.projet3.Puzzle.model.City;
import fr.eql.AI110.projet3.Puzzle.model.Job;

import java.security.NoSuchAlgorithmException;
import java.util.List;

public interface CandidateServiceInterface {
    Candidate create(Candidate candidate);
    Candidate getById(long id);
    Candidate connect(String mail, String password) throws NoSuchAlgorithmException;
    List<Candidate> getAllCandidate();
    List<Candidate> getAllVisibleCandidate();
    List<Candidate> getAllFiltredCandidate(Integer min, Integer max, Long diplomaId, Long periodId);
    Candidate getByMail(String mail);
    List<Candidate> filterCandidatesByCityAndRadius (List<Candidate> candidates, City city, int radius);
    Candidate getCandidateHiredOnThisJob(Job job);
}
