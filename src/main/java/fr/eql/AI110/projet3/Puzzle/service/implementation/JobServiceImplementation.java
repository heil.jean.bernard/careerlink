package fr.eql.AI110.projet3.Puzzle.service.implementation;

import fr.eql.AI110.projet3.Puzzle.model.*;
import fr.eql.AI110.projet3.Puzzle.repository.RecruitmentProcessRepository;
import fr.eql.AI110.projet3.Puzzle.repository.StepInterfaceRepository;
import fr.eql.AI110.projet3.Puzzle.service.JobServiceInterface;
import fr.eql.AI110.projet3.Puzzle.repository.JobInterfaceRepository;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class JobServiceImplementation implements JobServiceInterface {

    @Autowired
    private JobInterfaceRepository jobInterfaceRepository;
    @Autowired
    private RecruitmentProcessRepository recruitmentProcessRepository;
    @Autowired
    private StepInterfaceRepository stepInterfaceRepository;
    @Autowired
    private RecruiterServiceImplementation recruiterServiceImplementation;

    @Override
    public boolean isActiveJob(Job job) {
        if (job.getEffectiveHiringDate() == null && job.getCloseDate()==null) return true;
        else return false;
    }

    @Override
    public List<Job> getAllJob() {
        return jobInterfaceRepository.findAll();
    }
    @Override
    public List<Job> getActiveJobs() {
        return jobInterfaceRepository.findAllByCloseDateIsNullAndEffectiveHiringDateIsNull();
    }
    @Override
    public List<Job> getJobsByCandidateId(Candidate candidate) {
        return jobInterfaceRepository.findByCandidateId(candidate);
    }
    @Override
    public List<Job> getJobsByRecruiterId(long id) {
        return jobInterfaceRepository.findByRecruiterId(id);
    }
    @Override
    public Job getById(long id) {
        return jobInterfaceRepository.findById(id);
    }
    @Override
    public Job saveJob(Job job) {
        return jobInterfaceRepository.save(job);
    }
    @Override
    public List<RecruitmentProcess> recrutedProcessOnThisJob(Job job) {
        return  recruitmentProcessRepository
                .findByStepId(stepInterfaceRepository
                        .findByJobIdAndStepNumber(job.getId(), 20)
                        .getId());
    }
    @Override
    public Job createJob(Job job) {

            Job jobSave = jobInterfaceRepository.save(job);
            Recruiter recruiter = recruiterServiceImplementation.getByJob(jobSave);

            if (job.getStepSet()==null) {
            log.info("Init new job {} avec l'id {}",job.getTitle(), job.getId());

            System.out.println(recruiter.getFirstName());
            System.out.println(recruiter.getLastName());
            System.out.println(recruiter.getMail());

           log.debug("adding Step 100 : 'sortie'");
            Step step1 = new Step(null,recruiter.getFirstName(),recruiter.getLastName(),"Sortie de process.",recruiter.getMail(),"Sortie",100,jobSave,null);
            stepInterfaceRepository.save(step1);

            log.debug("adding Step 1 : 'Attente retours candidat'");
            Step step2 = new Step(null,recruiter.getFirstName(),recruiter.getLastName(),"Le poste à été proposé à un candidat et ce dernier doit répondre.",recruiter.getMail(),"Attente retours candidat",1,jobSave,null);
            stepInterfaceRepository.save(step2);

            log.debug("adding Step 2 : 'Candidatures à traiter'");
            Step step3 = new Step(null,recruiter.getFirstName(),recruiter.getLastName(),"Liste des candidatures reçues",recruiter.getMail(),"Candidatures à traiter",2,jobSave,null);
            stepInterfaceRepository.save(step3);

            log.debug("adding Step 19 : 'Proposition envoyée'");
            Step step4 = new Step(null,recruiter.getFirstName(),recruiter.getLastName(),"Une proposition de recrutement à été envoyée",recruiter.getMail(),"Proposition envoyée",19,jobSave,null);
            stepInterfaceRepository.save(step4);

            log.debug("adding Step 20 : 'Candidat recruté'");
            Step step5 = new Step(null,recruiter.getFirstName(),recruiter.getLastName(),"Le candidat est recruté",recruiter.getMail(),"Candidat recruté",20,jobSave,null);
            stepInterfaceRepository.save(step5);
        }

        log.debug("saving job");
        Job job2 = jobInterfaceRepository.findById(jobSave.getId()).get();

        log.info("job {} save",job2.getTitle());

        return job2;
    }


    @Override
    public List<Job> filterJobsByCityAndRadius(List<Job> jobs, City city, int radius) {
        log.info("Starting filter by radius and position");
        List<Job> jobInRadius = new ArrayList<Job>();
        double rad = Math.PI / 180;
        for (Job job : jobs) {
            log.debug("Checking distance...");
            double dist = (Math.acos(Math.sin(city.getLatitude() * rad) * Math.sin(job.getCity().getLatitude() * rad)
                    + Math.cos(city.getLatitude() * rad) * Math.cos(job.getCity().getLatitude() * rad)
                    * Math.cos(job.getCity().getLongitude() * rad - city.getLongitude() * rad))
                    * 6378.137);

            if (dist > radius) log.debug("Job is out of distance, not keeping...");
            else{
                log.debug("Job is in radius, keeping...");
                jobInRadius.add(job);
            }

            log.debug("job list size :"+jobs.size());
        }
        log.info("filter over, job list size :"+jobs.size());
        return jobInRadius;
    }
    @Override
    public List<Job> filterJobs(Integer min, Integer max, Long studyLevelId, Long workingTimeId, Long contractTypeId, String title, String details) {
        log.info(" Changing value of title and detail to match with the search : {} {}", title, details);

        log.info(" Checking title : {}", title);
        if (title!=null && title!="" && title!="null"){
            title='%'+title.trim()+'%';
            log.info(" Changing title to : {}", title);
        }

        if (details!=null && details!="" && details!="null"){
            log.info(" Checking detail : {}", title);
            details='%'+details.trim()+'%';
            log.info(" Changing details to : {}", details);
        }

        return jobInterfaceRepository.findByMulticriteria(min,max,studyLevelId,workingTimeId,contractTypeId,title,details);
    }


}

