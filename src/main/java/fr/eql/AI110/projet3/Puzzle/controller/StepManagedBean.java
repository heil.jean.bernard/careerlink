/**
 * This controller is related to step management inside job
 * It's used after getting a list of job, to manage steps
 * It's used in the job view detail for the management of a job by a recruiter
 *
 * It's also used in candidate dashboard to modelize the process
 *
 * @version 1.0
 *
 * @author Jean-Bernard HEIL
 */

package fr.eql.AI110.projet3.Puzzle.controller;


import fr.eql.AI110.projet3.Puzzle.model.*;
import fr.eql.AI110.projet3.Puzzle.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Slf4j
@Controller(value = "mbStep")
@Scope(value = "session")
public class StepManagedBean implements Serializable {


    private Long id;
    private String referentFirstName;
    private String referentLastName;
    private String details;
    private String referentMail;
    private String name;
    private String stepNumber;
    private List<RecruitmentProcess> stepProcesses;
    private String processFirstName;
    private Step step =new Step();
    private int index = 0;


    //endregion
    @Autowired
    private CandidateServiceInterface candidateServiceInterface;
    @Autowired
    private ResumeServiceInterface resumeServiceInterface;
    @Autowired
    private JobServiceInterface jobServiceInterface;

    @Autowired
    private StepServiceInterface stepServiceInterface;
    @Autowired
    private RecruitmentProcessServiceInterface recruitmentProcessServiceInterface;

    //region Methods

    /**
     * Method to get all the actives processes linked to a step.
     *
     * It allows you to know the number of candidates in process for the step,
     * and also the details of these applications.
     *
     * @return list of recruitment processes
     */
    public List<RecruitmentProcess> stepProcesses(String id) {
        System.out.println("Searching process with id : " + id);
        return recruitmentProcessServiceInterface.getAllActiveByStepId(Long.parseLong(id));
    }


    /**
     * Method to get all the actives processes linked to a step.
     *
     * It allows you to know the number of candidates in process for the step,
     * and also the details of these applications.
     *
     * @return list of recruitment processes
     */
    public String RedirectToDetailJob(long id){
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        session.setAttribute("currentJob",jobServiceInterface.getById(id));
        index =0;
        return "/job.xhtml?faces-redirection=true";
    }

    /**
     * When a job is offered to a candidate,
     * He has the possibility of validating it or not.
     * this method is then called.
     *
     * @return link to myProcess view
     */
    public String validStepByCandidate(long jobId){
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
       Candidate candidate= (Candidate) session.getAttribute("connectedCandidate");

       //Create a process with the second step
       recruitmentProcessServiceInterface.createProcess(new RecruitmentProcess(null,LocalDate.now(),null,null,
               "Candidature à étudier par la recruteur suite à validation du candidat",
               candidate,stepServiceInterface.GetStepByJobIdAndStepNumber(jobId, 2),resumeServiceInterface.getAllResumeByIdCandidate(candidate.getId()).get(0)));

       //Validate actual step
        RecruitmentProcess recruitmentProcess = recruitmentProcessServiceInterface.actualProcess(candidate.getId(),jobId);
        recruitmentProcess.setOkDate(LocalDate.now());
        recruitmentProcessServiceInterface.createProcess(recruitmentProcess);


      return "/myProcess.xhtml?faces-redirection=true";
    }

    /**
     * When a job is offered to a candidate,
     * He has the possibility of validating it or not.
     * this method is then called.
     *
     * @return link to myProcess view
     */
    public String invalidStepByCandidate(long jobId){
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        Candidate candidate = (Candidate) session.getAttribute("connectedCandidate");

        //Create a process with the exit step
        recruitmentProcessServiceInterface.createProcess(new RecruitmentProcess(
                null,
                LocalDate.now(),
                null,
                null,
                "Sortie du process par le candidat",
                candidate,
                stepServiceInterface.GetStepByJobIdAndStepNumber(jobId, 100),
                resumeServiceInterface.getAllResumeByIdCandidate(candidate.getId()).get(0)));

        //Invalidate actual step
        RecruitmentProcess recruitmentProcess = recruitmentProcessServiceInterface.actualProcess(candidate.getId(),jobId);
        recruitmentProcess.setNotDate(LocalDate.now());
        recruitmentProcessServiceInterface.createProcess(recruitmentProcess);

        return "/myProcess.xhtml?faces-redirection=false";
    }

    /**
     *Method used by recruiter to manage job.
     * valide the candidate on the step
     *
     * @return link to myProcess view
     */
    public String validStep() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        log.debug("Getting candidate from session...");
        Candidate candidate= (Candidate) session.getAttribute("currentCandidate");
        log.debug("Getting job from session...");
        Job job = (Job) session.getAttribute("currentJob");
        long jobId = job.getId();
        log.debug("Searching actual step for candidate : {} and for the job {]", candidate.getId(), jobId);

        //set the index to redirect on the right tabview's view
        log.debug("Setting the index");
        index = stepServiceInterface.getPosition(jobId,stepServiceInterface.actualStep(candidate.getId(),jobId))+1;

        //Create a process with the next step)
        log.debug("Saving the new process");
        recruitmentProcessServiceInterface.createProcess(new RecruitmentProcess(
                            null,LocalDate.now(),
                            null,
                            null,
                            "Le responsable à validé la candidature à l'étape précédente",
                            candidate,
                            stepServiceInterface.getNextStep(jobId,stepServiceInterface.actualStep(candidate.getId(),jobId)),
                            resumeServiceInterface.getAllResumeByIdCandidate(candidate.getId()).get(0)));

        //Validate actual step
        log.debug("Validate actual step");
        RecruitmentProcess recruitmentProcess = recruitmentProcessServiceInterface.actualProcess(candidate.getId(),jobId);
        recruitmentProcess.setOkDate(LocalDate.now());
        recruitmentProcessServiceInterface.createProcess(recruitmentProcess);

        //If step is final step close the Job and put the new job in session

        if (jobServiceInterface.recrutedProcessOnThisJob(jobServiceInterface.getById(job.getId())).size()>0){
            log.debug("Step is final, closing the job...");
            job.setEffectiveHiringDate(LocalDate.now());
            session.setAttribute("currentJob",jobServiceInterface.saveJob(job));
        }

        return "/job.xhtml?faces-redirection=true";
    }

    /**
     *Method used by recruiter to manage job.
     * invalide the candidate on the step
     *
     * @return link to myProcess view
     */
    public String invalidStep() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        Candidate candidate= (Candidate) session.getAttribute("currentCandidate");

        Job job = (Job) session.getAttribute("currentJob");
        long jobId = job.getId();

        //set the index
        index = stepServiceInterface.getPosition(jobId,stepServiceInterface.actualStep(candidate.getId(),jobId))+1;

        //Create a process with the exit step)
        recruitmentProcessServiceInterface.createProcess(new RecruitmentProcess(null,LocalDate.now(),null,null,
                "Le responsable n'a pas validé la candidature à l'étape précédente",
                candidate,stepServiceInterface.GetStepByJobIdAndStepNumber(jobId, 100),resumeServiceInterface.getAllResumeByIdCandidate(candidate.getId()).get(0)));

        //Invalidate actual step
        RecruitmentProcess recruitmentProcess = recruitmentProcessServiceInterface.actualProcess(candidate.getId(),jobId);
        recruitmentProcess.setNotDate(LocalDate.now());
        recruitmentProcessServiceInterface.createProcess(recruitmentProcess);

        return "/job.xhtml?faces-redirection=true";
    }

    /**
     *
     * First of three methods that aim to model the candidate's recruitment process.
     * this method returns the finished steps
     *
     * @return a list of step, those finish by the candidate
     */
    public List<Step> finishedStep(long jobId){;
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        Candidate candidate = (Candidate) session.getAttribute("connectedCandidate");
        return stepServiceInterface.finishedStep(candidate.getId(), jobId);
    }

    /**
     *
     * second of three methods that aim to model the candidate's recruitment process.
     * this method returns the actual step
     *
     * It's also used for the candidate dashboard
     *
     * @return a step
     */
    public Step actualStep(long jobId){
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        Candidate candidate = (Candidate) session.getAttribute("connectedCandidate");
        return stepServiceInterface.actualStep(candidate.getId(), jobId);
    }

    /**
     *
     * Third of three methods that aim to model the candidate's recruitment process.
     * this method returns the step to take to finish the job
     *
     * @return a list of step
     */
    public List<Step> toDoStep(long jobId){
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        Candidate candidate = (Candidate) session.getAttribute("connectedCandidate");
        return stepServiceInterface.todoStep(candidate.getId(), jobId);
    }

    /**
     *
     * Method to get all steps for a job.
     *
     * @return a list of step
     */
    public List<Step> stepList(long id){
        return stepServiceInterface.getAllByJobId(id);
    }

    /**
     *
     * Method to get number of candidate on a step.
     * Used in the recruiter dashboard
     *
     * @return a list of step
     */
    public int numberOfActualCandidate(long id){
       if (recruitmentProcessServiceInterface.getAllActiveByStepId(id)==null) return 0;
       else return recruitmentProcessServiceInterface.getAllActiveByStepId(id).size();
}
    //endregion

    //region Accessors


    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public Step getStep() {
        return step;
    }

    public void setStep(Step step) {
        this.step = step;
    }

    public String getProcessFirstName() {
        return processFirstName;
    }

    public void setProcessFirstName(String processFirstName) {
        this.processFirstName = processFirstName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReferentFirstName() {
        return referentFirstName;
    }

    public void setReferentFirstName(String referentFirstName) {
        this.referentFirstName = referentFirstName;
    }

    public String getReferentLastName() {
        return referentLastName;
    }

    public void setReferentLastName(String referentLastName) {
        this.referentLastName = referentLastName;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getReferentMail() {
        return referentMail;
    }

    public void setReferentMail(String referentMail) {
        this.referentMail = referentMail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStepNumber() {
        return stepNumber;
    }

    public void setStepNumber(String stepNumber) {
        this.stepNumber = stepNumber;
    }

    public List<RecruitmentProcess> getStepProcesses() {
        return stepProcesses;
    }

    public void setStepProcesses(List<RecruitmentProcess> stepProcesses) {
        this.stepProcesses = stepProcesses;
    }


    //endregion
}
