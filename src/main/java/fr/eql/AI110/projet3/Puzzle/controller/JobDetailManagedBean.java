/**
 * This controller is special for job creation
 *
 * @version 1.0
 *
 * for other functionality relatives to job
 * @see fr.eql.AI110.projet3.Puzzle.controller.JobRestController
 * @see fr.eql.AI110.projet3.Puzzle.controller.JobManagedBean
 * @author Jean-Bernard HEIL
 */


package fr.eql.AI110.projet3.Puzzle.controller;

import fr.eql.AI110.projet3.Puzzle.model.*;
import fr.eql.AI110.projet3.Puzzle.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.Min;
import java.io.Serializable;
import java.util.List;

@Slf4j
@Controller(value ="mbJobDetail")
@Scope(value = "request")
public class JobDetailManagedBean implements Serializable {


    private List<WorkingTime> times;
    private List<StudyLevel> levels;
    private List<ContractType> contracts;
    private List<Recruiter> referents; //Ici faire une requete avec le recruteur en session, pour aller chercher tous les recruteur étant dans la même société que lui.

    private ContractType jobContractType;
    private WorkingTime jobWorkingTime;
    private StudyLevel jobStudyLevel;
    private Recruiter recruiter;
    private Job job;
    private List<Step> steps;


    private Long id;
    private Long jobReferentId;
    private String jobCodePost;
    private String jobCity;
    private String title;
    private String details;
    private String jobLevel;
    private int maxSalary;

    private int minSalary;
    private String jobContract;
    private String jobTime;



    @Autowired
    private StudyLevelServiceInterface studyLevelServiceInterface;
    @Autowired
    private WorkingTimeServiceInterface workingTimeServiceInterface;
    @Autowired
    private ContractTypeServiceInterface contractTypeServiceInterface;
    @Autowired
    private JobServiceInterface jobServiceInterface;
    @Autowired
    private RecruiterServiceInterface recruiterServiceInterface;
    @Autowired
    private StepServiceInterface stepServiceInterface;



    //region Methods

    @PostConstruct
    public void init() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
//      recruiter = (Recruiter) session.getAttribute("connectedRecruiter");
        job = (Job) session.getAttribute("currentJob");
        steps = stepServiceInterface.getAllByJobId(job.getId());
        recruiter = recruiterServiceInterface.getById(1);
        referents = recruiterServiceInterface.GetAllByIdCompany(recruiter.getCompany().getId());
        times = workingTimeServiceInterface.getAllWorkingTime();
        levels = studyLevelServiceInterface.getAllStudyLevel();
       contracts = contractTypeServiceInterface.getAllContractType();
       maxSalary = job.getMaxSalary();
       minSalary = job.getMinSalary();
       jobLevel = job.getStudyLevel().getLevel();
       jobTime = job.getWorkingTime().getType();
       jobContract = job.getContractType().getType();
       details = job.getDetails();

    }

    /**
     * This method is using to change job details
     * Controller's bean attributes is used to change job attribute and save it.
     * JPA update already existing job.
     * @return the link to the job detail page, with the good index (0) the first one.
     */
    public String save(){

        log.debug("Saving the new job with input field");
        job.setRecruiter(recruiterServiceInterface.getById(jobReferentId));
        job.setMinSalary(minSalary);
        job.setMaxSalary(maxSalary);
        job.setStudyLevel(studyLevelServiceInterface.getByLevel(jobLevel));
        job.setContractType(contractTypeServiceInterface.getByType(jobContract));
        job.setWorkingTime(workingTimeServiceInterface.getByType(jobTime));
        job.setDetails(details);

        jobServiceInterface.saveJob(job);

        log.debug("set the step index to 0 to reach the good Tabview's view after redirection");
        StepManagedBean stepManagedBean = new StepManagedBean();
        stepManagedBean.setIndex(0);

        log.info("{] update !", job.getTitle());

        return "/job.xhtml?faces-redirection=true";
    }


    //endregion

    //region Accessors


    public List<Step> getSteps() {
        return steps;
    }

    public void setSteps(List<Step> steps) {
        this.steps = steps;
    }

    public List<WorkingTime> getTimes() {
        return times;
    }

    public void setTimes(List<WorkingTime> times) {
        this.times = times;
    }

    public List<StudyLevel> getLevels() {
        return levels;
    }

    public void setLevels(List<StudyLevel> levels) {
        this.levels = levels;
    }

    public List<ContractType> getContracts() {
        return contracts;
    }

    public void setContracts(List<ContractType> contracts) {
        this.contracts = contracts;
    }

    public List<Recruiter> getReferents() {
        return referents;
    }

    public void setReferents(List<Recruiter> referents) {
        this.referents = referents;
    }

    public ContractType getJobContractType() {
        return jobContractType;
    }

    public void setJobContractType(ContractType jobContractType) {
        this.jobContractType = jobContractType;
    }

    public WorkingTime getJobWorkingTime() {
        return jobWorkingTime;
    }

    public void setJobWorkingTime(WorkingTime jobWorkingTime) {
        this.jobWorkingTime = jobWorkingTime;
    }

    public StudyLevel getJobStudyLevel() {
        return jobStudyLevel;
    }

    public void setJobStudyLevel(StudyLevel jobStudyLevel) {
        this.jobStudyLevel = jobStudyLevel;
    }

    public Recruiter getRecruiter() {
        return recruiter;
    }

    public void setRecruiter(Recruiter recruiter) {
        this.recruiter = recruiter;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getJobReferentId() {
        return jobReferentId;
    }

    public void setJobReferentId(Long jobReferentId) {
        this.jobReferentId = jobReferentId;
    }

    public String getJobCodePost() {
        return jobCodePost;
    }

    public void setJobCodePost(String jobCodePost) {
        this.jobCodePost = jobCodePost;
    }

    public String getJobCity() {
        return jobCity;
    }

    public void setJobCity(String jobCity) {
        this.jobCity = jobCity;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getJobLevel() {
        return jobLevel;
    }

    public void setJobLevel(String jobLevel) {
        this.jobLevel = jobLevel;
    }

    public int getMaxSalary() {
        return maxSalary;
    }

    public void setMaxSalary(int maxSalary) {
        this.maxSalary = maxSalary;
    }

    public int getMinSalary() {
        return minSalary;
    }

    public void setMinSalary(int minSalary) {
        this.minSalary = minSalary;
    }

    public String getJobContract() {
        return jobContract;
    }

    public void setJobContract(String jobContract) {
        this.jobContract = jobContract;
    }

    public String getJobTime() {
        return jobTime;
    }

    public void setJobTime(String jobTime) {
        this.jobTime = jobTime;
    }

    //endregion
}
