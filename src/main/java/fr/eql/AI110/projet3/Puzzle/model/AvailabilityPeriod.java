/**
 * This class is using as a referential to describe the available period for candidates
 * Each candidate got an availability period
 *
 * @version 1.0
 *
 * @see fr.eql.AI110.projet3.Puzzle.model.Candidate
 * @author Jean-Bernard HEIL
 */

package fr.eql.AI110.projet3.Puzzle.model;

import javax.persistence.*;
import java.util.Set;


@Entity
@Table (name = "availabilityPeriod")
public class AvailabilityPeriod {
    //region Properties
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column (name = "name")
    private String name;
    //endregion

    //region Relational
    @OneToMany(mappedBy = "availabilityPeriod", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Candidate> candidateSet;
    //endregion

    //endregion

    //region Accessors
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Candidate> getCandidateSet() {
        return candidateSet;
    }

    public void setCandidateSet(Set<Candidate> candidateSet) {
        this.candidateSet = candidateSet;
    }
    //endregion

    //region Methods
    //endregion


    public AvailabilityPeriod() {
    }

    public AvailabilityPeriod(Long id, String name, Set<Candidate> candidateSet) {
        this.id = id;
        this.name = name;
        this.candidateSet = candidateSet;
    }
}
