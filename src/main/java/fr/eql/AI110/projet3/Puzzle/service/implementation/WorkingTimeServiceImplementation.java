package fr.eql.AI110.projet3.Puzzle.service.implementation;

import fr.eql.AI110.projet3.Puzzle.service.WorkingTimeServiceInterface;
import fr.eql.AI110.projet3.Puzzle.model.WorkingTime;
import fr.eql.AI110.projet3.Puzzle.repository.WorkingTimeInterfaceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WorkingTimeServiceImplementation implements WorkingTimeServiceInterface {

    @Autowired
    private WorkingTimeInterfaceRepository workingTimeInterfaceRepository;


    @Override
    public List<WorkingTime> getAllWorkingTime() {
        return workingTimeInterfaceRepository.findAll();
    }

    @Override
    public WorkingTime getByType(String type) {
        return workingTimeInterfaceRepository.findByType(type);
    }

    @Override
    public WorkingTime getById(long id) {
        return workingTimeInterfaceRepository.findById(id);
    }
}
