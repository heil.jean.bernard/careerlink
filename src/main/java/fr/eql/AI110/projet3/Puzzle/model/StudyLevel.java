/**
 * This class is using as a referential to reference all the study level selectable in the application (Licence, Master,...)
 *
 * @version 1.0
 *
 * @see fr.eql.AI110.projet3.Puzzle.model.Candidate
 * @see fr.eql.AI110.projet3.Puzzle.model.Job
 * @see fr.eql.AI110.projet3.Puzzle.model.Recruiter
 * @see fr.eql.AI110.projet3.Puzzle.model.Company
 *
 * @author Jean-Bernard HEIL
 */

package fr.eql.AI110.projet3.Puzzle.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table (name = "studylevel")
public class StudyLevel {
    //region Properties
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "level")
    private String level;
    //endregion

    //region Relational
    @OneToMany(mappedBy = "studyLevel", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Candidate> candidateSet;
    @OneToMany(mappedBy = "studyLevel", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Job> jobSet;
    //endregion

    //region Constructors



    public StudyLevel() {
    }
    public StudyLevel(Long id, String level, Set<Candidate> candidateSet, Set<Job> jobSet) {
        this.id = id;
        this.level = level;
        this.candidateSet = candidateSet;
        this.jobSet = jobSet;
    }
    //endregion

    //region Accessors
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Candidate> getCandidateSet() {
        return candidateSet;
    }

    public void setCandidateSet(Set<Candidate> candidateSet) {
        this.candidateSet = candidateSet;
    }

    public Set<Job> getJobSet() {
        return jobSet;
    }

    public void setJobSet(Set<Job> jobSet) {
        this.jobSet = jobSet;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
    //endregion

    //region Methods

    //endregion
}
