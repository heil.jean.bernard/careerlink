package fr.eql.AI110.projet3.Puzzle.service;

import fr.eql.AI110.projet3.Puzzle.model.AvailabilityPeriod;

import java.util.List;

public interface AvailabilityServiceInterface {

    List<AvailabilityPeriod> getAllAvailabilityPeriods();
    AvailabilityPeriod getById(long id);
}
