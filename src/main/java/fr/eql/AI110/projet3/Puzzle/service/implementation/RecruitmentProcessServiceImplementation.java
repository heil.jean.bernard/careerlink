package fr.eql.AI110.projet3.Puzzle.service.implementation;

import fr.eql.AI110.projet3.Puzzle.model.Job;
import fr.eql.AI110.projet3.Puzzle.model.RecruitmentProcess;
import fr.eql.AI110.projet3.Puzzle.model.Step;
import fr.eql.AI110.projet3.Puzzle.repository.RecruitmentProcessRepository;
import fr.eql.AI110.projet3.Puzzle.service.RecruitmentProcessServiceInterface;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class RecruitmentProcessServiceImplementation implements RecruitmentProcessServiceInterface {

    @Autowired
    private RecruitmentProcessRepository recruitmentProcessRepository;
    @Autowired
    private StepServiceImplementation stepServiceImplementation;

    @Override
    public List<RecruitmentProcess> getAllProcessByStepId(long id) {
        return recruitmentProcessRepository.findByStepId(id);
    }

    @Override
    public RecruitmentProcess createProcess(RecruitmentProcess recruitmentProcess) {

        return recruitmentProcessRepository.save(recruitmentProcess);
    }

    @Override
    public List<RecruitmentProcess> getAllActiveByStepId(long id) {
        if (recruitmentProcessRepository.findByStepIdAndNotDateIsNullAndOkDateIsNull(id).size()<1) return null;
        else return recruitmentProcessRepository.findByStepIdAndNotDateIsNullAndOkDateIsNull(id);
    }

    @Override
    public List<RecruitmentProcess> getAllActiveByJobId(long id) {
        List<RecruitmentProcess> result = new ArrayList<RecruitmentProcess>();
        for (Step step :
               stepServiceImplementation.getAllByJobId(id) ) {
           if (recruitmentProcessRepository.findByStepIdAndNotDateIsNullAndOkDateIsNull(step.getId())!=null){
               for (RecruitmentProcess r :
                       recruitmentProcessRepository.findByStepIdAndNotDateIsNullAndOkDateIsNull(step.getId())) {
                   result.add(r);
               }
           }
        }
        return result;
    }

    @Override
    public List<RecruitmentProcess> getAllByCandidateId(long id) {
        return recruitmentProcessRepository.findByCandidateId(id);
    }

    @Override
    public boolean  IsThatCandidateIsOnThisJob(long idCandidate, long idJob) {
boolean result = false;

for (Step step:
             stepServiceImplementation.getAllByJobId(idJob)) {
               if (recruitmentProcessRepository.findByCandidateIdAndStepId(idCandidate,step.getId()).size()>0) {
                   result=true;
                   break;
               }
        }
        return result;
    }

    @Override
    public RecruitmentProcess actualProcess(long candidateId, long jobId) {
            List<Step> allSteps = stepServiceImplementation.getAllByJobId(jobId);
            RecruitmentProcess result = new RecruitmentProcess();
            result.setDetails("erreur");

            for (Step step:allSteps) {
                if (recruitmentProcessRepository.findByCandidateIdAndStepIdAndNotDateIsNullAndOkDateIsNull(candidateId,step.getId())!=null) {
                    result=recruitmentProcessRepository.findByCandidateIdAndStepIdAndNotDateIsNullAndOkDateIsNull(candidateId,step.getId());
                    break;
                }
            }
            return result;
        }


}
