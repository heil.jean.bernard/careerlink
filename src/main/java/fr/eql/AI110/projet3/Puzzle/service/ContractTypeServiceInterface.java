package fr.eql.AI110.projet3.Puzzle.service;

import fr.eql.AI110.projet3.Puzzle.model.ContractType;

import java.util.List;

public interface ContractTypeServiceInterface {

    List<ContractType> getAllContractType();
    ContractType getByType(String type);
    ContractType getById(long id);
}
