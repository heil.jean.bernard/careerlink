package fr.eql.AI110.projet3.Puzzle.repository;

import fr.eql.AI110.projet3.Puzzle.model.Resume;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ResumeInterfaceRepository extends JpaRepository<Resume, Long> {
    List<Resume> findAll();
   Resume findById(long id);
    List<Resume> findAllByCandidateId(long id);
}
