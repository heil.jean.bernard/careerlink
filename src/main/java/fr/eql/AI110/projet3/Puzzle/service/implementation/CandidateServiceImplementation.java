package fr.eql.AI110.projet3.Puzzle.service.implementation;

import fr.eql.AI110.projet3.Puzzle.model.City;
import fr.eql.AI110.projet3.Puzzle.model.Job;
import fr.eql.AI110.projet3.Puzzle.model.RecruitmentProcess;
import fr.eql.AI110.projet3.Puzzle.repository.RecruitmentProcessRepository;
import fr.eql.AI110.projet3.Puzzle.service.CandidateServiceInterface;
import fr.eql.AI110.projet3.Puzzle.model.Candidate;
import fr.eql.AI110.projet3.Puzzle.repository.CandidateInterfaceRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;


@Service
@Slf4j
public class CandidateServiceImplementation implements CandidateServiceInterface {
    @Autowired
    private CandidateInterfaceRepository candidateRepository;
    @Autowired
    private LoginServiceImplementation loginServiceImplementation;
    @Autowired
    private StepServiceImplementation stepServiceImplementation;
    @Autowired
    private RecruitmentProcessRepository recruitmentProcessRepository;

    @Override
    public Candidate create(Candidate candidate) {
        log.info("Saving new candidate : {} {}", candidate.getFirstName(), candidate.getLastName());
        return candidateRepository.save(candidate);
    }

    @Override
    public Candidate getById(long id) {
        return candidateRepository.findById(id);
    }

    @Override
    public Candidate connect(String mail, String password) throws NoSuchAlgorithmException {
        log.info("Trying to connect as candidate : {} ", mail);

        String hashedPassword = loginServiceImplementation.hashAndSaltPassword(password, mail);
        System.out.println(hashedPassword);
        Candidate candidate = candidateRepository.findByMailAndHashedPassword(mail, hashedPassword);
        if (candidate!=null){
            log.info("Success to connect as candidate : {} ", mail);
            return candidate;
        } else {
            log.warn("Fail to connect as candidate : {} ", mail);
            return null;
        }
    }

    @Override
    public List<Candidate> getAllCandidate() {
        return candidateRepository.findAll();
    }

    @Override
    public List<Candidate> getAllVisibleCandidate() {
        return candidateRepository.findAllByVisibleIsTrue();
    }

    @Override
    public List<Candidate> getAllFiltredCandidate(Integer min, Integer max, Long diplomaId, Long periodId) {
        return candidateRepository.findByMulticriteria(min, max, diplomaId, periodId);
    }

    @Override
    public Candidate getByMail(String mail) {
        return candidateRepository.findByMail(mail);
    }

    @Override
    public List<Candidate> filterCandidatesByCityAndRadius(List<Candidate> candidates, City city, int radius) {
        List<Candidate> candidateInRadius = new ArrayList<Candidate>();
        double rad = Math.PI / 180;
        for (Candidate candidate : candidates) {
            System.out.println("Checking distance...");
            double dist = (Math.acos(Math.sin(city.getLatitude() * rad) * Math.sin(candidate.getCity().getLatitude() * rad)
                    + Math.cos(city.getLatitude() * rad) * Math.cos(candidate.getCity().getLatitude() * rad)
                    * Math.cos(candidate.getCity().getLongitude() * rad - city.getLongitude() * rad))
                    * 6378.137);

            if (dist > radius) System.out.println("candidate is out of distance, not keeping...");
            else{
                System.out.println("candidate is in radius, keeping...");
                candidateInRadius.add(candidate);
            }

            System.out.println("candidate list size :"+candidates.size());
        }
        System.out.println("filter over, candidate list size :"+candidates.size());
        return candidateInRadius;
    }

    public Candidate getCandidateHiredOnThisJob(Job job){
     List<RecruitmentProcess> processes = recruitmentProcessRepository.findByStep(stepServiceImplementation.GetStepByJobIdAndStepNumber(job.getId(), 20));
     return processes.get(0).getCandidate();
    }
}
