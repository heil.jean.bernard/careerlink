package fr.eql.AI110.projet3.Puzzle.service.implementation;

import fr.eql.AI110.projet3.Puzzle.model.AvailabilityPeriod;
import fr.eql.AI110.projet3.Puzzle.repository.AvailabilityPeriodInterfaceRepository;
import fr.eql.AI110.projet3.Puzzle.service.AvailabilityServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AvailabilityPeriodImplementation implements AvailabilityServiceInterface {

    @Autowired
    private AvailabilityPeriodInterfaceRepository availabilityPeriodInterfaceRepository;

    @Override
    public List<AvailabilityPeriod> getAllAvailabilityPeriods() {
        return availabilityPeriodInterfaceRepository.findAll();
    }

    @Override
    public AvailabilityPeriod getById(long id) {
        return availabilityPeriodInterfaceRepository.findById(id);
    }
}
