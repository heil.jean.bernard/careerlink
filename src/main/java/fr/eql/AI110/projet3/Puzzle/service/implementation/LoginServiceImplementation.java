package fr.eql.AI110.projet3.Puzzle.service.implementation;

import fr.eql.AI110.projet3.Puzzle.service.LoginServiceInterface;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Service
public class LoginServiceImplementation implements LoginServiceInterface

{
    @Override
    public String hashAndSaltPassword(String password, String mail) throws NoSuchAlgorithmException {

        MessageDigest msg = MessageDigest.getInstance("SHA-256");

        //Hash mail
        byte[] hash = msg.digest(mail.getBytes(StandardCharsets.UTF_8));

        //convert to hexadecimal
        StringBuilder salt = new StringBuilder();
        for (byte b : hash) {
            salt.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
        }

        //salt password with hashed hexa mail
        password = salt.toString()+password+salt.toString();

        //hash salted password
        hash = msg.digest(password.getBytes(StandardCharsets.UTF_8));
        StringBuilder hashAndSalt = new StringBuilder();
        for (byte b : hash) {
            hashAndSalt.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
        }

        return hashAndSalt.toString();
    }
}
