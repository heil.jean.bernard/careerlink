package fr.eql.AI110.projet3.Puzzle.service.implementation;

import fr.eql.AI110.projet3.Puzzle.service.StudyLevelServiceInterface;
import fr.eql.AI110.projet3.Puzzle.model.StudyLevel;
import fr.eql.AI110.projet3.Puzzle.repository.StudyLevelInterfaceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudyLevelServiceImplementation implements StudyLevelServiceInterface {

    @Autowired
    private StudyLevelInterfaceRepository studyLevelInterfaceRepository;

    @Override
    public List<StudyLevel> getAllStudyLevel() {
        return studyLevelInterfaceRepository.findAll();
    }

    @Override
    public StudyLevel getById(long id) {
        return studyLevelInterfaceRepository.findById(id);
    }

    @Override
    public StudyLevel getByLevel(String level) {
        return studyLevelInterfaceRepository.findByLevel(level);
    }
}
