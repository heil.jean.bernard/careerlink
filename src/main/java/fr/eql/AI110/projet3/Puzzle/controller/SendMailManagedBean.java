/**
 * This controller is to send Email
 * Not used in this version ut usefull to infor candidate and recruiter
 * of change in process
 *
 * @see fr.eql.AI110.projet3.Puzzle.service.EmailSenderService
 *
 * @version 1.0
 *
 * @author Jean-Bernard HEIL
 */

package fr.eql.AI110.projet3.Puzzle.controller;
import javax.mail.MessagingException;

import fr.eql.AI110.projet3.Puzzle.service.EmailSenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class SendMailManagedBean {

    @Autowired
    private EmailSenderService service;

    /**
     * Request to send mail
     */
    @ResponseBody
    @RequestMapping("/sendHtmlEmail")
    public String sendHtmlEmail() throws MessagingException {

            service.sendSimpleEmail("careerlinkspring@gmail.com",
                    "<h1>This email has attachment</h1>",
                       "Email from Puzzle");

            return "Email send to";
    }


}