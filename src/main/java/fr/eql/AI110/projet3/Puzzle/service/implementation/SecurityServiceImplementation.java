package fr.eql.AI110.projet3.Puzzle.service.implementation;

import fr.eql.AI110.projet3.Puzzle.service.SecurityServiceManagement;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class SecurityServiceImplementation implements SecurityServiceManagement {


    @Override
    public String removeSpecialChar(String string) {

        if (string!="null" && string!=null && string!="" && string !=" ") {
            log.debug("Checking string security for input : {}", string);
            string = string.trim().replaceAll("[^a-zA-Z0-9éôàè]", " ");
            log.debug("Check done, return : {}", string);
            return string;
        }
        return null;
    }

    @Override
    public boolean isFormatSalaryIsOk(String salary) {
        if (salary.matches("^(?:[1-9][0-9]{4,5})$")) return true;
        else return false;
    }

    @Override
    public boolean isMailFormatOk(String mail) {
        if (mail.matches("^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$")) return true;
                return false;
    }
}
