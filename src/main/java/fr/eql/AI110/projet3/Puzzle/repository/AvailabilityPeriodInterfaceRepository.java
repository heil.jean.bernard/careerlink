package fr.eql.AI110.projet3.Puzzle.repository;

import fr.eql.AI110.projet3.Puzzle.model.AvailabilityPeriod;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AvailabilityPeriodInterfaceRepository extends JpaRepository<AvailabilityPeriod, Long> {

    List<AvailabilityPeriod> findAll();
    AvailabilityPeriod findById(long id);
}
