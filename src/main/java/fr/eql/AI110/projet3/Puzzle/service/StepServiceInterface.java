package fr.eql.AI110.projet3.Puzzle.service;

import fr.eql.AI110.projet3.Puzzle.model.Job;
import fr.eql.AI110.projet3.Puzzle.model.Step;

import java.util.List;

public interface StepServiceInterface {

    List<Step> getAllStep();
    List<Step> getAllByJobId(long id);
    Step saveStep(Step step);
    Step getStepById(long id);
    Step GetStepByJobIdAndStepNumber(long id, int number);
    List<Step> finishedStep(long idCandidate, long idJob);
    Step actualStep(long idCandidate, long idJob);
    List<Step> todoStep(long idCandidate, long idJob);
    Step getNextStep(long jobId, Step step);
    int getPosition(long jobId, Step step);
    boolean addStepToJob(Job jobToAddStep, String stepName, String firstName, String lastName, String detail, String mail);
    List<Step> getStepListWithoutStep1AndStep100(Job job);

}
