package fr.eql.AI110.projet3.Puzzle.service;

import fr.eql.AI110.projet3.Puzzle.model.City;

public interface CityServiceInterface {

    City saveCity(City city);
    City getCityByPostCodeAndCityName(String postCode, String cityName);
}
