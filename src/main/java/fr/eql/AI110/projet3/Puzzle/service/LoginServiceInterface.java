package fr.eql.AI110.projet3.Puzzle.service;

import java.security.NoSuchAlgorithmException;

public interface LoginServiceInterface {

String hashAndSaltPassword(String password, String mail) throws NoSuchAlgorithmException;

}
