package fr.eql.AI110.projet3.Puzzle.repository;

import fr.eql.AI110.projet3.Puzzle.model.WorkingTime;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface WorkingTimeInterfaceRepository extends JpaRepository<WorkingTime, Long> {
    List<WorkingTime> findAll();
   WorkingTime findByType(String type);
   WorkingTime findById(long id);
}
