package fr.eql.AI110.projet3.Puzzle.repository;

import fr.eql.AI110.projet3.Puzzle.model.Job;
import fr.eql.AI110.projet3.Puzzle.model.Step;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface StepInterfaceRepository extends JpaRepository<Step, Long> {

    List<Step> findAll();
    List<Step> findAllByJobId(long id);
    Step findById(long id);
    Step findByJobIdAndStepNumber(long id, int number);

    @Query("SELECT s FROM Step s INNER JOIN FETCH s.job j WHERE j =:job and s.stepNumber <> 100 AND s.stepNumber <> 1")
    List<Step> findAllStepWithoutStep1AndStep100ByJob(@Param("job") Job job);
}
