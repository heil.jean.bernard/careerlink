package fr.eql.AI110.projet3.Puzzle.repository;

import fr.eql.AI110.projet3.Puzzle.model.ContractType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ContractTypeInterfaceRepository extends JpaRepository<ContractType, Long> {

    List<ContractType> findAll();
    ContractType findByType(String type);
    ContractType findById(long id);
}
