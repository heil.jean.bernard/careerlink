/**
 * This class represent a Job in the application
 * A job has its own properties and can be completed by multiple steps
 * It's linked with a recruiter
 *
 * @version 1.0
 *
 * @see fr.eql.AI110.projet3.Puzzle.model.City
 * @see fr.eql.AI110.projet3.Puzzle.model.Recruiter
 * @see fr.eql.AI110.projet3.Puzzle.model.StudyLevel
 * @see fr.eql.AI110.projet3.Puzzle.model.ContractType
 * @see fr.eql.AI110.projet3.Puzzle.model.WorkingTime
 * @see fr.eql.AI110.projet3.Puzzle.model.Step

 * @author Jean-Bernard HEIL
 */

package fr.eql.AI110.projet3.Puzzle.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "job")
public class Job {
    //region Properties
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "title")
    private String title;
    @Column(name = "details", columnDefinition="LONGTEXT")
    private String details;
    @Column(name = "maxsalary")
    private int maxSalary;
    @Column(name = "minsalary")
    private int minSalary;
    @Column(name = "creationdate")
    private LocalDate creationDate;
    @Column(name = "effectivehiringdate")
    private LocalDate effectiveHiringDate;
    @Column(name = "Closedate")
    private LocalDate closeDate;
    //endregion

    //region Relational

    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private City city;
    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private Recruiter recruiter;
    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private StudyLevel studyLevel;
    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private ContractType contractType;
    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private WorkingTime workingTime;
    @OneToMany(mappedBy = "job", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Step> stepSet;
    //endregion

    //region Constructors
    public Job() {
    }

    public Job(Long id, String title, String details, int maxSalary, int minSalary, LocalDate creationDate, LocalDate effectiveHiringDate, LocalDate closeDate, City city, Recruiter recruiter, StudyLevel studyLevel, ContractType contractType, WorkingTime workingTime, List<Step> stepSet) {
        this.id = id;
        this.title = title;
        this.details = details;
        this.maxSalary = maxSalary;
        this.minSalary = minSalary;
        this.creationDate = creationDate;
        this.effectiveHiringDate = effectiveHiringDate;
        this.closeDate = closeDate;
        this.city = city;
        this.recruiter = recruiter;
        this.studyLevel = studyLevel;
        this.contractType = contractType;
        this.workingTime = workingTime;
        this.stepSet = stepSet;
    }
    //endregion

    //region Accessors
    public String getTitle() {
        return title;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public int getMaxSalary() {
        return maxSalary;
    }

    public void setMaxSalary(int maxSalary) {
        this.maxSalary = maxSalary;
    }

    public int getMinSalary() {
        return minSalary;
    }

    public void setMinSalary(int minSalary) {
        this.minSalary = minSalary;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDate getEffectiveHiringDate() {
        return effectiveHiringDate;
    }

    public void setEffectiveHiringDate(LocalDate effectiveHiringDate) {
        this.effectiveHiringDate = effectiveHiringDate;
    }

    public LocalDate getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(LocalDate closeDate) {
        this.closeDate = closeDate;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Recruiter getRecruiter() {
        return recruiter;
    }

    public void setRecruiter(Recruiter recruiter) {
        this.recruiter = recruiter;
    }

    public StudyLevel getStudyLevel() {
        return studyLevel;
    }

    public void setStudyLevel(StudyLevel studyLevel) {
        this.studyLevel = studyLevel;
    }

    public ContractType getContractType() {
        return contractType;
    }

    public void setContractType(ContractType contractType) {
        this.contractType = contractType;
    }

    public WorkingTime getWorkingTime() {
        return workingTime;
    }

    public void setWorkingTime(WorkingTime workingTime) {
        this.workingTime = workingTime;
    }

    public List<Step> getStepSet() {
        return stepSet;
    }

    public void setStepSet(List<Step> stepSet) {
        this.stepSet = stepSet;
    }
    //endregion

    //region Methods

    //endregion


}
