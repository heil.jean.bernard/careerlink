package fr.eql.AI110.projet3.Puzzle.service;

public interface SecurityServiceManagement {

   String removeSpecialChar(String string);
   boolean isFormatSalaryIsOk(String string);
   boolean isMailFormatOk(String mail);
}
