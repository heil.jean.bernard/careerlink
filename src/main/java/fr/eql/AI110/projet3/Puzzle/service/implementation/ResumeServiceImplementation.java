package fr.eql.AI110.projet3.Puzzle.service.implementation;

import fr.eql.AI110.projet3.Puzzle.model.Resume;
import fr.eql.AI110.projet3.Puzzle.repository.ResumeInterfaceRepository;
import fr.eql.AI110.projet3.Puzzle.service.ResumeServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ResumeServiceImplementation implements ResumeServiceInterface {

    @Autowired
    private ResumeInterfaceRepository resumeInterfaceRepository;


    @Override
    public List<Resume> getAllResume() {
        return resumeInterfaceRepository.findAll();
    }

    @Override
    public Resume getByid(long id) {
        return resumeInterfaceRepository.findById(id);
    }

    @Override
    public List<Resume> getAllResumeByIdCandidate(long id) {
        return resumeInterfaceRepository.findAllByCandidateId(id);
    }

    @Override
    public Resume createResume(Resume resume) {
        return resumeInterfaceRepository.save(resume);
    }


}
