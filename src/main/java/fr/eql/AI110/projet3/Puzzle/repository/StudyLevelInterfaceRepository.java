package fr.eql.AI110.projet3.Puzzle.repository;

import fr.eql.AI110.projet3.Puzzle.model.StudyLevel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StudyLevelInterfaceRepository extends JpaRepository<StudyLevel, Long> {

    List<StudyLevel> findAll();
    StudyLevel findById(long id);
    StudyLevel findByLevel(String level);
}
